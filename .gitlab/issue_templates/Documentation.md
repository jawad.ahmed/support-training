---
module-name: "Documentation"
area: "Product Knowledge"
gitlab-group: "Create:Editor"
maintainers:
  - cynthia
  - greg
---

### Overview

**Goal**: You feel proficient at making a documentation merge request.

*Length*: hours (maybe? please update with a better estimate)

**Objectives**: At the end of this module, you should be able to:

- Open a merge request specifically for documentation.
- Follow the documentation process.
- Find who the Technical Writer counterpart for the relevant group the merge request is related to.

### Stage 0: Create Your Module

1. [ ] Create an issue using this template by making the Issue Title: `Documentation Training - <your name>`.
1. [ ] Add yourself as the assignee.
1. [ ] Set milestones, if applicable, and a due date to help motivate yourself!

Consider using the Time Tracking functionality so that the estimated length for the module can be refined.

### Stage 1: Introduction

1. [ ] Watch this [introduction to docs video](https://www.youtube.com/watch?v=BlaZ65-b7y0) ([Slides](https://docs.google.com/presentation/d/11-_w_iIhucMbdZ5ybKAFFGpqDAOtWepP5TZ3RtgU8Ik/edit#slide=id.g3db14447a7_0_0)) to get a quick overview. Please keep in mind this was made for external contributors.
1. [ ] Read about [the Docs-first methodology](https://docs.gitlab.com/ee/development/documentation/styleguide/index.html#docs-first-methodology) and [why our documentation is the single source of truth](https://docs.gitlab.com/ee/development/documentation/styleguide/index.html#documentation-is-the-single-source-of-truth-ssot).
1. [ ] Familiarize yourself with the [Ticket deflection through documentation Support workflow](https://about.gitlab.com/handbook/support/workflows/how-to-respond-to-tickets.html#documentation).

#### Where to ask for help

If at any time, you have questions or need help getting a MR started or completed, please reach out.

- [#docs](https://gitlab.slack.com/archives/C16HYA2P5)
- [#git-help](https://gitlab.slack.com/archives/C1E21S2LD)
  - specifically for `git` command line issues
- [#support_team-chat](https://gitlab.slack.com/archives/CCBJYEWAW)
  - You can ping those listed as knowledgeable in "Documentation" or one of the Docs counterparts.

### Stage 2: Your first docs MR!

There is a lot of information surrounding documentation and how to do it, but the best way to learn is to start with a small MR and get right into it.

1. [ ] Find a super small fix to do. This might be a typo, format issue, small wording change. For this first MR, try not to pick a technical change.
    - If you can't find anything, you can also consider a [documentation issue that's good for new contributors](https://gitlab.com/gitlab-org/gitlab/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=documentation&label_name[]=good%20for%20new%20contributors).
1. [ ] Make the change via whatever editor you want (local, WebIDE, Web Editor).
    - Tip: While viewing the page on the docs site, scroll to the bottom and click on "Edit this page" to view the repo file.
    - When looking at the repository, if you want to know which webpage it is, search for the title of the page on `docs.gitlab.com`. Note: The navigation is not 1:1 with the folder structure, so you cannot rely on that to find the page.
    - Branch name should start with `docs-` (or [one of other options listed in the branch name scheme](https://docs.gitlab.com/ee/development/documentation/#branch-naming)).
    - Make sure the commit message follows our [commit message guidelines](https://docs.gitlab.com/ee/development/contributing/merge_request_workflow.html#commit-messages-guidelines), in particular:
        1. 3 or more words.
        1. Start with a capital letter.
        1. No period at the end.
1. [ ] When you open the merge request, [as per our docs MR guidelines](https://docs.gitlab.com/ee/development/documentation/#merge-requests-for-gitlab-documentation), make sure to choose "Documentation" as the template.
1. [ ] Fill in the template as best you can, the minimum:
    1. Description on change.
    1. Link to ticket/issue if relevant.
    1. Add appropriate labels. Minimum:
        - documentation (should be automatically added via the "Documentation" template, but check to make sure). Note: This label is also used to track your contributions to documentation, so make sure to add it!
        - group [scoped label](https://docs.gitlab.com/ee/user/project/labels.html#scoped-labels) (if in doubt, it should be listed at the top of the docs page)
        - customer (if related to a ticket)
        - Note: stage, section, [Support Team Contributions](https://about.gitlab.com/handbook/support/#support-fixes), and docs-only labels should be added by the bot if you don't add them.
        - [backporting labels](https://about.gitlab.com/handbook/engineering/releases/#self-managed-releases-1) if the change needs to be included in stable releases
    1. Follow the [MR guidelines](https://docs.gitlab.com/ee/development/code_review.html) and set the relevant Technical Writer (TW) as a Reviewer.
        - The documentation template will also have instructions on how to find this, but basically refer to the metadata information at the top of the source version of the docs page (this will tell you which stage/group you want to refer to if you're unsure).
    1. Make sure that the following options are checked:
        - `Delete source branch when merge request is accepted.`
        - `Squash commits when merge request is accepted.` "Apply suggestion" is used frequently in docs MRs so this is preferred.
1. [ ] Submit!
    - Your first docs MR: LINK HERE
    - Once it's been reviewed by the assigned TW, you'll have finished your first docs MR. Congratulations!

### Stage 3: Guidelines

Let's now take the time to read up on some of the relevant guidelines in a bit more detail:

1. [ ] Go over the [Documentation workshop slides from Contribute 2020](https://docs.google.com/presentation/d/1QrO-XQ7RfZ6OD29lRt5L42dxOoJFEienag_VRS_rwsE/edit#slide=id.g55e9452978_0_75).
1. [ ] All documentation guidelines are under the [Contributor > Documentation section](https://docs.gitlab.com/ee/development/documentation/). You don't need to read everything, but take a browse through to know what's there.
1. [ ] Read through the [documentation process](https://docs.gitlab.com/ee/development/documentation/workflow.html).
    - Note: Support is expected to verify the technical accuracy of a docs MR before assigning to a TW. When in any doubt, get a technical review from an engineer first.
1. [ ] Read the [post merge review guidelines](https://docs.gitlab.com/ee/development/documentation/workflow.html#post-merge-reviews).
    - When would you use this in Support: Got an urgent docs MR? At times, we may want a docs fix as soon as possible.
    - If it's in the "Troubleshooting section" of any page, follow the guidelines except assign to a support manager who is online who will do a quick review and merge.
    - If it's not, then depending on how quickly you need it, share it in the #docs Slack channel asking anyone there to review and merge ASAP. This does not require a post-merge review, as it involves a TW review.
1. [ ] Read about [docs deploy](https://docs.gitlab.com/ee/development/documentation/site_architecture/index.html#deploy-the-docs-site). The key thing is to note how often docs are deployed.
    1. [ ] Optionally, read the rest of the [site architecture information](https://docs.gitlab.com/ee/development/documentation/site_architecture/index.html) to learn how the docs site is built.

#### Style and linters

1. [ ] Complete the [GitLab Technical Writing Fundamentals](https://levelup.gitlab.com/learn/course/gitlab-technical-writing-fundamentals) course on LevelUp (~2.5 hours duration). Collect your badge!
1. [ ] Read the [documentation style guide](https://docs.gitlab.com/ee/development/documentation/styleguide/).
    - No one expects you to memorize all the style guidelines! Read through it once and try to remember what's covered so that you can reference it later.
1. [ ] Briefly read over the [topic types page](https://docs.gitlab.com/ee/development/documentation/topic_types/) with a more careful read of the [troubleshooting section](https://docs.gitlab.com/ee/development/documentation/topic_types/troubleshooting.html).
1. [ ] While not required, it is highly recommended to [install the docs linters](https://docs.gitlab.com/ee/development/documentation/testing.html#local-linters) in your local editor to prevent pipelines from failing, or use the GDK. Plugins can be also used from the command line.
    1. [ ] Set up [markdownlint](https://docs.gitlab.com/ee/development/documentation/testing.html#markdownlint).
    1. [ ] Set up [Vale](https://docs.gitlab.com/ee/development/documentation/testing.html#vale).
    1. [ ] [Set up a vertical ruler](https://stackoverflow.com/questions/29968499/vertical-rulers-in-visual-studio-code) to help you [split long lines](https://docs.gitlab.com/ee/development/documentation/styleguide/index.html#text).

### Stage 4: From tickets to documentation

Whenever a customer says the docs are confusing or unclear, we should be making a merge request to resolve their concern.
If you're not sure what to put in the MR, then you can instead create a documentation issue

1. [ ] Review 5 tickets where documentation was created as a result of the ticket. You can search in [merged documentation support contribution MRs](https://gitlab.com/gitlab-org/gitlab/-/merge_requests?scope=all&utf8=%E2%9C%93&state=merged&label_name[]=documentation&label_name[]=Support%20Team%20Contributions). Note that not all MRs with the gitlab-org/gitlab~"Support Team Contributions" label originate from Zendesk tickets, so if you don't see a link to a ticket, you may want to review a different MR.
    1. [ ] __
    1. [ ] __
    1. [ ] __
    1. [ ] __
    1. [ ] __
1. [ ] Read over how the [Document this](https://about.gitlab.com/handbook/support/workflows/improving-documentation.html) function in ZenDesk works.
    - You can search in ZenDesk for `tags:doc_issue_created` to locate such tickets.
1. [ ] Alternatively, you can [create issues with the documentation template](https://gitlab.com/gitlab-org/gitlab/-/issues/new?issuable_template=Documentation) manually.
1. Make at least 2 docs MRs. Ideally, they are part of working on a customer ticket. If so, remember to include a link to the ZD ticket in the MR description. If you cannot find a ticket with an open docs update, you can create any update that meets one of these criteria:
    1. [ ] Docs MR that adds content (usually troubleshooting section): LINK
    1. [ ] Docs MR that fixes inaccuracy: LINK
1. [ ] Optional: In addition to what was previously covered, add the appropriate [scoped docs label](https://gitlab.com/groups/gitlab-org/-/labels?utf8=%E2%9C%93&subscribed=&search=docs%3A%3A) _and_ a milestone to your MRs. Choose the current milestone unless it is less than 5 days away. Otherwise, use the next milestone.

### Penultimate Stage: Review

You feel that you can now do all of the objectives:

- open a merge request specifically for documentation.
- follow the documentation process.
- find who the Technical Writer counterpart for the relevant group the merge request is related to.

Any updates or improvements needed? If there are any dead links, out of date or inaccurate content, missing content whether in this module or in other documentation, list it below as tasks for yourself! Once ready, have a [maintainer or manager review](https://gitlab.com/gitlab-com/support/support-training#guideline-to-update-support-training-module).

- [ ] Update ...

### Final Stage: Completion

1. [ ] Have your trainer and manager review this issue.
1. [ ] Manager: schedule a call (or integrate into 1:1) to review how the module went once you have reviewed this issue.
1. [ ] Submit a MR to update `modules` and `knowledge_areas` in the [Support Team yaml file](https://gitlab.com/gitlab-com/support/team/-/blob/master/data/support-team.yaml) with this training module's topic. You will now be listed as an expert in this topic on [Skills by Person page](https://gitlab-com.gitlab.io/support/team/skills-by-person.html).
1. [ ] Consider also taking the [Code Contributions training](https://gitlab.com/gitlab-com/support/support-training/-/blob/master/.gitlab/issue_templates/Code%20Contributions.md) which focuses on GitLab code changes.

/label ~module
/label ~"Module::Documentation"
