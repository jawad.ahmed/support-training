---
module-name: "Kubernetes Part 2"
area: "Product Knowledge"
gitlab-group: "Enablement:Distribution"
maintainers:
  - ckaburu
---

## Overview

This is a continuation in the Kubernetes Series. Remember to contribute to any documentation that needs updating.

**Prerequisites**: kubernetes-part-1

**Goal**: Set a clear path for start of your Kubernetes journey.

**Objectives**: At the end of this module, you should be able to:
- <insert a few concrete actions that learner should be able to do at the end of this module>

## Stage 0: Pre-Requisites

1. [ ] You have completed [Kubernetes Part 1 - Introduction to Kubernetes](https://gitlab.com/gitlab-com/support/support-training/-/blob/master/.gitlab/issue_templates/Kubernetes%20Part%201.md) module.
[ ] You have completed [Helm and GitLab Charts](https://gitlab.com/gitlab-com/support/support-training/-/blob/master/.gitlab/issue_templates/Helm%20and%20GitLab%20Charts.md) module.
1. [ ] Create an issue using this template by making the Issue Title: Kubernetes Module Series II: GitLab Cloud Native Charts - <your name>
1. [ ] Add yourself and your trainer as the assignees.
1. [ ] Notify your manager on the issue to let them know them you have started.
1. [ ] Optional: Set a milestone, if applicable, and a due date to help motivate yourself!
1. [ ] Get Invited to a recurrent Calendar event or create one in your region if none exists yet, to pair and learn together.

## Stage 1: Background Knowledge

This stage is meant to provide your with detailed background knowledge to get you started.

**Helm Charts**
1. [ ] [How To Create Your First Helm Chart](https://docs.bitnami.com/kubernetes/how-to/create-your-first-helm-chart/)
1. [ ] [Building Helm Charts From the Ground Up: An Introduction to Kubernetes](https://www.youtube.com/watch?v=vQX5nokoqrQ)
1. [ ] [Helm from basics to advanced](https://banzaicloud.com/blog/creating-helm-charts/)
1. [ ] [The Chart Template Developer’s Guide](https://helm.sh/docs/chart_template_guide/)
1. [ ] [Introduction to Helm 3](https://helm.sh/blog/helm-3-released/)

**GitLab Cloud Native Charts**
1. [ ] [Architecture of Cloud native GitLab Helm charts](https://gitlab.com/gitlab-org/charts/gitlab/blob/master/doc/architecture/index.md)
1. [ ] [Configuring Chart Globals](https://gitlab.com/gitlab-org/charts/gitlab/blob/master/doc/charts/globals.md)
1. [ ] [Deployment Guide](https://gitlab.com/gitlab-org/charts/gitlab/blob/master/doc/installation/deployment.md)
1. [ ] [Storage Guide](https://gitlab.com/gitlab-org/charts/gitlab/blob/master/doc/installation/storage.md)
1. [ ] [Troubleshooting](https://gitlab.com/gitlab-org/charts/gitlab/tree/master/doc/troubleshooting)

## Stage 2: Hands-on Exercises
These exercises are meant to give hands-on experience

**Basics**
1. [ ] [Setup GKE Resources](https://gitlab.com/gitlab-org/charts/gitlab/blob/master/doc/installation/cloud/gke.md) & Deploy GitLab (See item 4)
1. [ ] [Setup EKS resources](https://gitlab.com/gitlab-org/charts/gitlab/blob/master/doc/installation/cloud/eks.md) & Deploy GitLab (See item 4)
1. [ ] [Manually Setting Up Secrets](https://gitlab.com/gitlab-org/charts/gitlab/blob/master/doc/installation/secrets.md)
1. [ ] [Deploying GitLab](https://gitlab.com/gitlab-org/charts/gitlab/blob/master/doc/installation/deployment.md)
1. [ ] [Upgrading GitLab](https://gitlab.com/gitlab-org/charts/gitlab/blob/master/doc/installation/upgrade.md)
1. [ ] [Migrating from Omnibus-GitLab package based installation](https://gitlab.com/gitlab-org/charts/gitlab/blob/master/doc/installation/migration/index.md)

**Intermediate**
1. [ ] [Deploying production-ready GitLab on Google Kubernetes Engine](https://cloud.google.com/solutions/deploying-production-ready-gitlab-on-gke)
1. [ ] [Using External DB](https://gitlab.com/gitlab-org/charts/gitlab/blob/master/doc/advanced/external-db/index.md)
1. [ ] [Using External Object Storage](https://gitlab.com/gitlab-org/charts/gitlab/blob/master/doc/advanced/external-object-storage/index.md)
1. [ ] [Using External Redis](https://gitlab.com/gitlab-org/charts/gitlab/blob/master/doc/advanced/external-redis/index.md)
1. [ ] [Managing Persistent Volumes after Install](https://gitlab.com/gitlab-org/charts/gitlab/blob/master/doc/advanced/persistent-volumes/index.md)
1. [ ] [Using ExternalDNS](https://gitlab.com/gitlab-org/charts/gitlab/blob/master/doc/installation/deployment.md#dynamic-ips-with-external-dns)


**Advanced**
1. [ ] [Using GitLab Geo with GitLab Helm Chart](https://gitlab.com/gitlab-org/charts/gitlab/tree/master/doc/advanced/geo)
1. [ ] [Using External Gitaly](https://gitlab.com/gitlab-org/charts/gitlab/tree/master/doc/advanced/external-gitaly)
1. [ ] [GitLab Operator](https://gitlab.com/gitlab-org/cloud-native/gitlab-operator)
1. [ ] Read blogs on GitLab Operator below (optional):
     - [ ] [How to install and use the GitLab Kubernetes Operator](https://about.gitlab.com/blog/2021/11/16/gko-on-ocp/)
     - [ ] [Test GitLab Operator for OpenShift](https://cloud.redhat.com/blog/test-the-new-gitlab-operator-for-openshift)

## Stage 3: Projects

1. [ ] Create a GKE cluster WITHOUT using the script provided in the docs, deploy GitLab with external Storage for all features that need object storage and manually create secrets.
    In the comments, provide the following:
    - The commands used to create all the secrets created
    - the final `values.yaml` file used
    - the output of `kubectl get all`

1. [ ] Create an EKS cluster [using the script](https://gitlab.com/gitlab-org/charts/gitlab/blob/master/doc/installation/cloud/eks.md) provided in the docs and setup [ExternalDNS using Route53](https://github.com/kubernetes-sigs/external-dns/blob/master/docs/tutorials/aws.md)
    In the comments, provide the following:
    - The output of the bootstrap script
    - The manifest file used to deploy ExternalDNS
    - A screenshot showing the DNS records created

## Stage 4: Pairing Sessions

1. [ ] The essence of this pairing sessions is to learn together with other team members while solving customer tickets. You are required to mention 5 pairing sessions you participated and your key take aways from the sessions in the comments area below. (Tick when you have 5)
  1. [ ] __
  1. [ ] __
  1. [ ] __
  1. [ ] __
  1. [ ] __

## Stage 5: Quiz?

- [ ] **Done with Stage 5**

_Need link to Quiz here_

- [ ] Quiz answers were checked by a Kubernetes expert (_insert name here_), who said you passed.

## Final Stage

1. [ ] Have your trainer review your tickets and assessment. If you do not have a trainer, ask an expert to review.
1. [ ] Manager: schedule a call (or integrate into 1:1) to review how the module went.

/label ~module
/label ~"Module::Kubernetes Part 2"
