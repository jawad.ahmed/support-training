---
module-name: "Customers Console"
area: "Customer Service"
maintainers:
  - cynthia
  - ralfaro
---

### Overview

Your manager's approval _must_ be obtained before starting this module.

**Requirements:**

- [ ] Basic Ruby, Ruby on Rails and [Ruby Interactive Shell](https://www.digitalocean.com/community/tutorials/how-to-use-irb-to-explore-ruby) knowledge are minimum requirements as all work is done in production Rails console environments.
- [ ] Completion of [Subscription, License, and Renewal training](https://gitlab.com/gitlab-com/support/support-training/-/blob/master/.gitlab/issue_templates/Subscriptions%20License%20and%20Renewals.md).

**Goal**: This mini-module is meant to provide guidance on getting started with customers portal console.

**Objectives**: At the end of this module, you should be able to:

- follow console related requests.
- resolve Support Engineer Escalations related to Customers Portal through common console related requests.

Start with Stage 1. Stages 2 and 3 can be done simulatenously.

## Stage 0: Create the module and get manager approval

1. [ ] After obtaining approval, assign yourself and your trainer to this issue.
1. [ ] Open an [access request](https://gitlab.com/gitlab-com/team-member-epics/access-requests/-/issues/new?issuable_template=Individual_Bulk_Access_Request) to request access to `ssh GitLab Customer Server`.
  1. [ ] Make note of [the instructions](https://gitlab.com/gitlab-org/customers-gitlab-com#accessing-production-as-an-admin-and-logs-and-console) for accessing the customers portal production console.
1. [ ] Optional: Set milestones, if applicable, and a due date to help motivate yourself!

### Stage 1: Get primed and ready

- [ ] **Done with Stage 1**

1. [ ] Have an existing Owner add you to the `gitlab-com/support/customers-console` group as an `Owner` so that you are a direct member.
1. [ ] Subscribe to new issues and/or merge requests for [support console project](https://gitlab.com/gitlab-com/support/toolbox/console-training-wheels).
1. [ ] Subscribe to the following labels (if not subscribed to dotcom-internal project):
    1. [`Console Escalation::Customers` label](https://gitlab.com/gitlab-com/support/internal-requests/-/labels?utf8=%E2%9C%93&subscribed=&search=customers+console).

### Stage 2: Related reading and resources


- [ ] **Done with Stage 2**

1. Review these resources:
    1. [ ] [Customers console function list](https://about.gitlab.com/handbook/support/license-and-renewals/workflows/customersdot/customer_console.html). Recommend bookmarking this one.
    1. [ ] [Familiarize yourself with the CustomersDot docs](https://gitlab.com/gitlab-org/customers-gitlab-com/-/tree/staging/doc) and particularly [Architecture](https://gitlab.com/gitlab-org/customers-gitlab-com/-/tree/staging/doc/architecture) with focus on the different systems that communication with the customers portal.
1. [ ] Review the [customers portal database structure](https://gitlab.com/gitlab-org/customers-gitlab-com/#database) to get a better understanding of how things are connected.
1. Review these customers portal issues to get a better understanding of what is often asked and how to troubleshoot.
    1. [ ] [Force reassocation](https://gitlab.com/gitlab-com/support/internal-requests/-/issues/1714) using cheatsheet command
    1. [ ] [Delete user account](https://gitlab.com/gitlab-com/support/internal-requests/-/issues/1764)
    1. [ ] [Setting specific values to nil as workaround to bug](https://gitlab.com/gitlab-com/support/internal-requests/-/issues/1749)
    1. [ ] [Rescuing an incomplete order](https://gitlab.com/gitlab-com/support/internal-requests/-/issues/1339)

### Stage 3: Working on issues

- [ ] **Done with Stage 3**

When taking any action, remember to copy and paste your console commands output to record what's been done. If needed, create an issue to record your actions.
Read-only actions don't necessarily need to be recorded, but any change actions do.

1. [ ] **Acknowledgement**: You acknowledge not to manually change any attributes in an order except for what is outlined as okay to change [in the manually changing attributes section](https://about.gitlab.com/handbook/support/license-and-renewals/workflows/customersdot/customer_console.html#manually-changing-attributes).
1. [ ] Pair on internal issues that require console access. Create a pairing session and link it here.
   1. Pairing:

1. [ ] Answer 3 internal issues that require console access and paste the links here. Remember to paste the commands you run (and output if needed) as a comment in the issue.

   1.
   1.
   1.

### Penultimate Stage: Review

Any updates or improvements needed? If there are any dead links, out of date or inaccurate content, missing content whether in this module or in other documentation, list the needed updates below as tasks for yourself! Once ready, have a [maintainer or manager review](https://gitlab.com/gitlab-com/support/support-training#guideline-to-update-support-training-module).

- [ ] Update ...

### Final Stage: Completion

1. [ ] Have your manager review this issue.
1. [ ] Manager: schedule a call (or integrate into 1:1) to review how the module went once you have reviewed this issue.
1. [ ] Submit a MR to update the [Support Team yaml file](https://gitlab.com/gitlab-com/support/team/-/blob/master/data/support-team.yaml) with this training module's topic under your list of completed `modules`.

/label ~module
/label ~"Module::Customers Console"
