---
module-name: "GitLab Dedicated Basics"
area: "Customer Service"
maintainers:
  - lyle
---

## Introduction

This training module provides Support Engineers with the basics of answering GitLab Dedicated product related tickets,
for both new and existing Support team members who are looking to start working on these tickets.

**Goals**

At the end of this module, you should be able to:
- Understand the differences between GitLab SaaS and Dedicated
- answer GitLab Dedicated questions
- know where to get additional help

**General Timeline and Expectations**

- This module should take you **1 week to complete**.
- Read about our [Support Onboarding process](https://about.gitlab.com/handbook/support/training/)

## Stage 0: Create Your Module

1. [ ] Create an issue using this template by making the Issue Title: `<module title> - <your name>`.
1. [ ] Add yourself and your trainer as the assignees.
1. [ ] Set milestones, if applicable, and a due date to help motivate yourself!
1. [ ] Update [`support-team.yaml`](https://gitlab.com/gitlab-com/support/team/-/blob/master/data/support-team.yaml) to indicate that you've started learning this knowledge area:  
   ```
   knowledge_areas:
   - name: GitLab Dedicated
     level: 1
   ```

Consider using the Time Tracking functionality so that the estimated length for the module can be refined.

## Stage 1: Overview - What is GitLab Dedicated?

- [ ] **Done with Stage 1**

1. [ ] Read the [GitLab Dedicated Overview page](https://gitlab-com.gitlab.io/gl-infra/gitlab-dedicated/team/).
1. [ ] The GitLab Dedicated Engineer On Call (GDEOC) will be your main point of contact for infrastructure-related issues. Read the [GDEOC on-call runbook](https://gitlab.com/gitlab-com/gl-infra/gitlab-dedicated/team/-/blob/main/runbooks/on-call.md) to learn more about the GDEOC's responsibilities.
1. [ ] Read the [GitLab Dedicated product category direction page](https://about.gitlab.com/direction/saas-platforms/dedicated/)
1. [ ] Read the [GitLab Dedicated Docs page](https://docs.gitlab.com/ee/subscriptions/gitlab_dedicated/)


## Stage 2: GitLab Dedicated Architecture and Troubleshooting

- [ ] **Done with Stage 2**

**Keep in mind:**

1. Only GitLab Support Engineers and SRE _do_ [have acccess to logs](#searching-logs), while GitLab Dedicated users do **not** have access to logging or the underlying infrastructure. Please do not send any doc links that are self-managed only and that talk about server-side configuration (check badging at the top of the page or section for **FREE/STARTER/PREMIUM/ULTIMATE ONLY**). Customers will have admin access to the GitLab UI, so anything from the `administration` section is acceptable.
1. Troubleshooting problems on Dedicated, SaaS, and Self-managed are the same or similar. The main difference is where and how we gather the necessary information.

### Architecture

1. Review materials relevant to GitLab Dedicated Architecture
    1. [ ] Read about the [GitLab Dedicated architecture](https://gitlab-com.gitlab.io/gl-infra/gitlab-dedicated/team/architecture/Architecture.html).
        - Note: You're not expected to remember everything, but to get a general sense of GitLab Dedicated's architecture. Feel free to read the details on any component based on your interest.
    1. [ ] GitLab Dedicated runs a modified [Cloud Native Hybrid Reference Architecture](https://docs.gitlab.com/ee/administration/reference_architectures/2k_users.html#cloud-native-hybrid-reference-architecture-with-helm-charts-alternative). Review the [Changes from Reference Architecture](https://gitlab-com.gitlab.io/gl-infra/gitlab-dedicated/team/architecture/Architecture.html#changes-from-reference-architectures) page. (This may be brief, if you are already familiar with the GitLab reference architecture).
1. Configs are stored in JSON files in the [switchboard_la project](https://gitlab.com/gitlab-com/gl-infra/gitlab-dedicated/sandbox/switchboard_la/-/tree/main/tenant_models)
    1. [ ] Browse through the JSON files to see what types of config settings can be added. In general, settings that can be modified in [GET](https://gitlab.com/gitlab-org/gitlab-environment-toolkit) can be added here.

### Searching logs

Access to logs is through credentials stored in the 1Password Vault. 

1. [ ] Access Opensearch by looking in the 1Password `Gitlab Dedicated - Support` Vault for the `opensearch` entries and access one of the URLs listed there.
    1. Select "**Global**" tenant
    1. Choose "**Discover**" at the sidebar under **OpenSearch** Dashboards
    1. On the next screen, you should see logs. Make sure that index called `gitlab-*` is selected.
1. [ ] Access Grafana by looking in the 1Password `Gitlab Dedicated - Support` Vault for the `grafana` entries and access one of the URLs listed there.

### Filing issues

**NOTE: Until General Availablity (GA), the GitLab Dedicated team is currently at its capacity and unable to answer requests in Slack.**
**If you need attention from the GitLab Dedicated team, open an issue in the issue tracker, do not ping the team or members of the team unless it's an absolute emergency.**

[Customer Support Requests are the highest non-paging priority](https://gitlab.com/gitlab-com/gl-infra/gitlab-dedicated/team/-/blob/main/runbooks/on-call.md#non-emergency-requests) for Dedicated SREs.

   - [ ] If an issue can only be resolved through rails console access, or needs additional troubleshooting from an SRE you can [raise a GitLab Dedicated Support Request](https://gitlab.com/gitlab-com/gl-infra/gitlab-dedicated/team/-/issues/new?issuable_template=support_request).
     - Direct link for opening an issue with the "Support Request" issue template: https://gitlab.com/gitlab-com/gl-infra/gitlab-dedicated/team/-/issues/new?issuable_template=support_request
     - For some requests (for example [creation of a private link](https://gitlab.com/gitlab-com/gl-infra/gitlab-dedicated/team/-/issues/new?issuable_template=private_link_request)) there might be a more specific issue template available; feel free to browse through the available templates for inspiration, but you can always default to the "Support Request" template if unsure.
     - If you pick a more specific issue template, please make sure that the following labels are set:
     ```
     /label "Dedicated::Support Request"
     /confidential
     /label "team::GitLab Dedicated"
     /label "workflow-infra::Ready"
     ```

### Escalating an Emergency issue

Emergencies from GitLab Dedicated will come through the [Customer Emergencies On-call Rotation](https://about.gitlab.com/handbook/support/workflows/customer_emergencies_workflows.html) as with other emergency types. 
1. [ ] Read the definition of an emergency in the [GDEOC runbook](https://gitlab.com/gitlab-com/gl-infra/gitlab-dedicated/team/-/blob/main/runbooks/on-call.md#what-is-an-emergency)
1. [ ] The GitLab Dedicated Infrastructure team has a 24/7 PagerDuty rotation: [GitLab Dedicated Platform Escalation](https://gitlab.pagerduty.com/schedules#PE57MNA). You can [manually create a PD Incident](https://about.gitlab.com/handbook/support/workflows/support_manager-on-call.html#manually-triggering-a-pagerduty-notification) using the [Dedicated Platform Service](https://gitlab.pagerduty.com/service-directory/P1H70IW) or use the Slack command `/pd trigger` and choose "Dedicated Platform Service" as the **Impacted Service** to escalate an emergency to an SRE after initial triage and analysis.

### Places to reproduce

When you need to reproduce an issue:

#### Troubleshooting GitLab Features

1. [ ] You have access to 2 GitLab.com groups for each plan tier `gitlab-gold` (Ultimate), `gitlab-silver` (Premium).
    - Since these are public groups, you can also link them in tickets as long as you make the project public as well.
    - If you find out that you do not have access to one of these groups, ask for help in `#support_gitlab-com` or `#support_operations` Slack channel.

#### Troubleshooting platform or reference architecture

1. [ ] As with Self-managed troubleshooting, you have access to [testing environments](https://about.gitlab.com/handbook/support/workflows/test_env.html). For GitLab Dedicated specifically, you'll likely be [testing in AWS](https://about.gitlab.com/handbook/support/workflows/test_env.html#aws-testing-environment) with the appropriate [GitLab Dedicated Architecture](#architecture). Remember, you can also [file an issue with Dedicated SREs](#filing-issues).

### Security issues

The [Gitlab Infrastructure Security team](https://about.gitlab.com/handbook/engineering/security/security-engineering-and-research/infrastructure-security/) focuses on instance wide security breaches and vulnerabilities. If you suspect a security issue, [engage the Security Incident Response Team](https://about.gitlab.com/handbook/engineering/security/security-operations/sirt/#-engaging-sirt).

## Stage 3: Keeping up to date

- [ ] **Done with Stage 3**

Keeping up to date and asking questions:

1. [ ] Check the [Support Week in Review](https://gitlab.com/gitlab-com/support/readiness/support-week-in-review/-/milestones) for recent GitLab.com `[Dedicated]` updates/issues.
1. [ ] Make sure you're in the `#f_gitlab_dedicated` Slack channel where you can ask questions about the product itself, tickets, process, etc. Also check the pinned messages in that channel for recent changes.
1. [ ] Consider joining `#g_dedicated-team` to interact with Dedicated SREs directly for any questions about a specific instance or Dedicated behavior.


## Final Stage

1. [ ] Manager: schedule a call (or integrate into 1:1) to review how the module went once you have reviewed this issue.
1. [ ] Please submit MRs for this Issue Template with any improvements that you have noticed.
1. [ ] Submit an MR to update your `knowledge_areas` slug in the [Support Team yaml file](https://gitlab.com/gitlab-com/support/team/-/blob/master/data/support-team.yaml) with this training module's topic.
   ```
   knowledge_areas:
   - name: GitLab Dedicated
     level: 2
   ```


/label ~module
/label ~"Module::GitLab Dedicated"
