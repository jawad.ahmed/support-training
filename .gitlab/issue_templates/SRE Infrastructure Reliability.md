---
module-name: "SRE Infrastructure (Reliability)"
area: "Product Knowledge"
gitlab-group: "Enablement:Infrastructure"
maintainers:
  - swainaina
---

<!---
This Training Module was inspired by one SRE Internship:
https://gitlab.com/gitlab-com/people-group/Training/-/issues/1390
--->

**Title:** _SRE Infrastructure (Reliability) - **your-name**"_

Preferably, follow the order of the stages, but it's not mandatory.

### Stage 1: Commit to learning about Site Reliability Engineering 

- [ ] **Done with Stage 1**

1. [ ] Notify your manager on the issue to let them know them you have started.
1. [ ] Notify the team via one of the Support Channels.

### Stage 1: Site Reliability Engineering

- [ ] SRE overview
  - [ ] [SRE Introduction](https://gitlab.com/gitlab-com/support/support-training/-/issues/2138) module
  - [ ] [SRE Infrastracture II (Intermediate)](https://gitlab.com/gitlab-com/support/support-training/-/issues/2139)
  
### Stage 1: Site Reliability Engineering with special focus to Realibility team
- [ ] Prometheus
  - [ ] Get familiar with the [Prometheus operator](https://github.com/prometheus-operator/prometheus-operator) which is used to set up the monitoring stack
    - [ ] Watch this video [Kubernetes Operator simply explained in 10 mins](https://www.youtube.com/watch?v=ha3LjlD6g7g)
    - [ ] Setup Prometheus Monitoring on Kubernetes using Helm and Prometheus Operator | [Part 1](https://www.youtube.com/watch?v=QoDqxm7ybLc&t=0s)
    - [ ] Deploy the [sample setup](https://github.com/prometheus-operator/prometheus-operator/tree/main/example/user-guides/getting-started)
  - [ ] Get familiar with [prometheus](https://thanos.gitlab.net/graph), investigate how to [query](https://prometheus.io/docs/querying/basics/) to get information out of it.
  - [ ] Get familiar with [prometheus alert manager](https://alerts.gitlab.net), checkout the documentation of this in the [runbooks](https://gitlab.com/gitlab-com/runbooks/-/blob/master/docs/monitoring/alerts_manual.md).
- [ ] [Helmfile](https://gitlab.com/gitlab-com/gl-infra/k8s-workloads/gitlab-helmfiles)
  - [ ] [Setup helmfile](https://github.com/roboll/helmfile)
  - [ ] [The Helmfile Best Practices Guide](https://github.com/roboll/helmfile/blob/master/docs/writing-helmfile.md)
  - [ ] Deploy monitoring on [minikube](https://gitlab.com/gitlab-com/gl-infra/k8s-workloads/gitlab-helmfiles#manual)
  - [ ] Setup [monitoring Calico component metrics](https://docs.projectcalico.org/archive/v3.21/maintenance/monitor/monitor-component-metrics) with prometheus
  - [ ] Review [gitlab-helmfiles](https://gitlab.com/gitlab-com/gl-infra/k8s-workloads/gitlab-helmfiles) repository
- [ ] [Tanka](https://tanka.dev/)
  - [ ] [Getting started with Tanka](https://gitlab.com/gitlab-com/gl-infra/k8s-workloads/tanka-deployments/-/blob/master/CONTRIBUTING.md#getting-started)
    - [ ] Jsonnet tutorial: https://jsonnet.org/learning/tutorial.html
    - [ ] Tanka tutorial: https://tanka.dev/tutorial/overview
    - [ ] Tanka inline environments documentation: https://tanka.dev/inline-environments
- [ ] [Configuration management using terraform](https://ops.gitlab.net/gitlab-com/gl-infra/config-mgmt)
    - [ ] [Getting Started with Terraform](https://ops.gitlab.net/gitlab-com/gl-infra/config-mgmt#getting-started)
       - [ ] [Terraform State](https://www.terraform.io/language/state)
    - [ ] [Creating a new environment](https://ops.gitlab.net/gitlab-com/gl-infra/config-mgmt#creating-a-new-environment)
    - [ ] Review [GKE modules](https://ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/gke) and [Docs](https://registry.terraform.io/providers/hashicorp/google/4.7.0)
- [ ] Thanos
  - [ ] Review this [Quick tutorial on Thanos](https://thanos.io/tip/thanos/quick-tutorial.md/)
  - [ ] Worth checking [Katacoda Thanos Course](https://katacoda.com/thanos/courses/thanos)
- [ ] [Grafana](https://grafana.com/docs/grafana/latest/getting-started/getting-started/?plcmt=footer)
- [ ] [Runbook](https://gitlab.com/gitlab-com/runbooks)
- [ ] [Read about Readiness review](https://about.gitlab.com/handbook/engineering/infrastructure/production/readiness/). Link a couple of readiness review issues that you find interesting
   - [ ] 
   - [ ] 
- [ ] [Change Management](https://about.gitlab.com/handbook/engineering/infrastructure/change-management/)
- [ ] [Milestone: Reliability Engineering | GitLab](https://about.gitlab.com/handbook/engineering/infrastructure/team/reliability/#milestone)

## Pairing Issues / MR

- [ ]
- [ ]

### Final Stage

- [ ] Your manager needs to check this box to acknowledge that you have finished.

/label ~module
/label ~"Module::SRE Infrastructure Reliability"
