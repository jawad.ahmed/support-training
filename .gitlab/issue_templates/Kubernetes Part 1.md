---
module-name: "Kubernetes Part 1"
area: "Core Technologies"
maintainers:
  - ckaburu
---

## Overview

This is a prerequisite to other Kubernetes modules in the series. Remember to contribute to any documentation that needs updating.

**Goal**: Set a clear path for start of your Kubernetes journey.

*Length*: <estimate # of hours, use single number or range>

**Objectives**: At the end of this module, you should be able to:
- Be familiar with k8s nomanclature, so that you can understand which components customers are referring to in tickets.
- Spin up and navigate a basic k8s cluster. Understand the output of basic commands, such as get and describe for services, pods, nodes, etc.

## Stage 0: Create and commit to the module

1. [ ] Create an issue using this template by making the Issue Title: Kubernetes Module Series I: Introduction to Kubernetes - <your name>
1. [ ] Add yourself and your trainer as the assignees.
1. [ ] Notify your manager to let them know you've started.
1. [ ] Commit to this by notifying the current experts that they can start routing non-technical Kubernetes questions to you.
1. [ ] Optional: Set a milestone, if applicable, and a due date to help motivate yourself!
1. [ ] Get Invited to a recurrent Calendar event or create one in your region if none exists yet, to pair and learn together.

## Stage 1: Background Knowledge

This stage is meant to provide your with detailed background knowledge to get you started.

1. [ ] Follow Tutorial: [Kubernetes Basics](https://kubernetes.io/docs/tutorials/kubernetes-basics/)
1. [ ] Read [Kubernetes Concepts](https://kubernetes.io/docs/concepts/overview)
1. [ ] [Study the Kubernetes Cheatsheet](https://kubernetes.io/docs/reference/kubectl/cheatsheet/)
1. [ ] [Helm QuickStart](https://helm.sh/docs/intro/quickstart/)
1. [ ] [Introduction to k3s](https://www.youtube.com/watch?v=FrGpJNI8na4)
    - [Try K3sup](https://github.com/alexellis/k3sup) a light-weight utility to get from zero to KUBECONFIG with k3s on any local or remote VM.
1. [ ] [Learn about Octant](https://octant.dev/) Octant is an open source developer-centric web interface for Kubernetes that lets you inspect a Kubernetes cluster and its applications. Extremely helpful while you're learning.
1. [ ] TroubleShooting Kubernetes
    - [Kubernetes.io Troubleshooting Documentation](https://kubernetes.io/docs/tasks/debug-application-cluster/troubleshooting/)
    - [A visual guide on troubleshooting Kubernetes deployments](https://learnk8s.io/troubleshooting-deployments)

## Stage 2: Hands-on Exercises
These exercises are meant to give hands-on experience. They can be accessed by signing-up to [O'Reilly platform](https://www.oreilly.com/)  with your personal email (gitlab domain emails will redirect to sign-in with Okta, and fail with "You're not assigned to this application" ). Monthly subscription cost is reimbursable as per this [issue](https://gitlab.com/gitlab-com/support/support-team-meta/-/issues/4453#note_1118168603).

**Basics**
1. [ ] [Launch a Single-node Kubernetes Cluster using Minikube](https://learning.oreilly.com/scenarios/launch-a-single/9781492062066/)
1. [ ] [Deploy containers using kubectl](https://learning.oreilly.com/scenarios/deploy-containers-to/9781492062059/)
1. [ ] [Deploy Containers using YAML](https://learning.oreilly.com/scenarios/deploy-containers-to/9781492061984/)
1. [ ] [Getting Started with Kubeadm](https://learning.oreilly.com/scenarios/kubernetes-fundamentals-kubeadm/9781098135201/)
1. [ ] [Helm Package Manager](https://learning.oreilly.com/scenarios/kubernetes-pipelines-helm/9781492078968/)

**Intermediate**
1. [ ] [Networking in Kubernetes](https://learning.oreilly.com/scenarios/get-started-with/9781492062097/)
1. [ ] [Ingress Routing](https://learning.oreilly.com/scenarios/create-kubernetes-ingress/9781492061977/)
1. [ ] [Running Stateful Services with Kubernetes (Storage)](https://learning.oreilly.com/scenarios/run-stateful-services/9781492062103/)
1. [ ] [Managing Secrets](https://learning.oreilly.com/scenarios/use-kubernetes-to/9781492062080/)
1. [ ] [Troubleshooting Kubernetes Applications](http://troubleshooting.kubernetes.sh/)

**Advanced**
1. [ ] [Kelsey Hightower's Kubernetes the hard way](https://github.com/kelseyhightower/kubernetes-the-hard-way)


## Stage 3: Projects

1. [ ] Google Cloud: Set up a Cluster using [gcloud command tool](https://cloud.google.com/kubernetes-engine/docs/how-to/creating-a-container-cluster); you have access to a Google Cloud project via [gitlabsandbox](https://gitops.gitlabsandbox.cloud/). Related questions can be directed to `#sandbox-cloud-questions` slack channel.

    In the Comments, provide the following:
    - The `gcloud` command used to create the cluster
    - Dump current cluster state

1. [ ] Create a cluster using [`eksctl` with Cluster Autoscaling enabled](https://docs.aws.amazon.com/eks/latest/userguide/cluster-autoscaler.html)
    In the comments, provide the following:
    - The command used to create the cluster
    - The output after the cluster is created
    - A tail of the Cluster AutoScaler logs

1. [ ] Deploy a sample application to a Cluster, with a Persistent Disk and Service using a YAML file
    In the Comments, provide the following:
    - Content of the YAML file used
    - Describe the Pod, Deployment and Service created

## Stage 4: Pairing Sessions

1. [ ] The essence of this pairing sessions is to learn together with other team members while solving customer tickets. You are required to mention 5 pairing sessions you participated and your key take aways from the sessions in the comments area below. (Tick when you have 5)
  1. [ ] __
  1. [ ] __
  1. [ ] __
  1. [ ] __
  1. [ ] __

## Stage 5: Quiz?

_Need link to Quiz here_

- [ ] Quiz answers were checked by a Kubernetes expert (_insert name here_), who said you passed.

## Penultimate stage: Review

Any updates or improvements needed? If there are any dead links, out of date or inaccurate content, missing content whether in this module or in other documentation, list it below as tasks for yourself! Once ready, have a [maintainer or manager review](https://gitlab.com/gitlab-com/support/support-training#guideline-to-update-support-training-module).

1. [ ] Update ...

## Final stage: Completion

1. [ ] Have your trainer review your tickets and assessment. If you do not have a trainer, ask an expert to review.
1. [ ] Manager: schedule a call (or integrate into 1:1) to review how the module went.
1. [ ] Submit a MR to update `modules` and `knowledge_areas` in the [Support Team yaml file](https://gitlab.com/gitlab-com/support/team/-/blob/master/data/support-team.yaml) with this training module's topic. You will now be listed as Kubernetes Expert on [Skills by Person page](https://gitlab-com.gitlab.io/support/team/skills-by-person.html).
1. [ ] Discuss taking the [CKA Examination](https://training.linuxfoundation.org/certification/certified-kubernetes-administrator-cka/) with your manager and Pass the Exam.

/label ~module
/label ~"Module::Kubernetes Part 1"
