---

module-name: "Support Hiring - Stage2 Technical Interview Training"
area: "Customer Service"

---

There are 2 different interview stages you can contribute to in the Support Hiring process.  This is the second of them - the Technical Interview.  (The first is the Take Home Assessment.) 

### Technical Interviews

- [ ] Review the [technical interview process](https://gitlab.com/gitlab-com/support/tech-interview/se-interview) and do a dry-run in creating the required resources.

- [ ] Open a [People Ops Interview Training](https://gitlab.com/gitlab-com/people-group/Training/-/issues/new?issuable_template=interview_training), make a merge request on the [Support Engineering Interview Process](https://gitlab.com/gitlab-com/people-group/hiring-processes/blob/master/Engineering/Support/Support%20Engineer/Interview%20Process.md) to add yourself to the **Interviewers with open interview trainings (tag in to shadow)** section and assign it to your manager for approval.
    - [ ] Add 2 checkboxes below your name in open interview training section mentioning `[ ] First Shadow (Interviewer) `, `[ ] Second Shadow (Interviewer)`, `[ ] Reverse Shadow`.

- [ ] Once your People Ops Interview Training is complete: Create a merge request on the [Support Engineering Interview Process](https://gitlab.com/gitlab-com/people-group/hiring-processes/blob/master/Engineering/Support/Support%20Engineer/Interview%20Process.md) to remove yourself to the **Interviewers with open interview trainings (tag in to shadow)** section and add yourself to the **Interviews** section and assign it to your manager. Make sure to ping [your region's technical recruiter](https://gitlab.com/gitlab-com/people-group/hiring-processes/-/blob/master/Engineering/Support/Support%20Engineer/Interview%20Process.md#peopleops-hiring-team) in the MR so that they are informed.
    - [ ] Make sure to keep your calendar up to date and set your scheduling preferences by logging into your Prelude account.
    
- [ ] [Create an Access Request](https://about.gitlab.com/handbook/business-technology/team-member-enablement/onboarding-access-requests/access-requests/#slack-google-groups-1password-vaults-or-groups-access-requests) to add yourself to `@supportinterviewers` Slack group.

- [ ] Go to [Prelude](https://about.gitlab.com/handbook/hiring/prelude/) and follow the instructions on how to change your `Interview Limits
` to **Daily limit**: 1 and **Weekly limit**: 2.


### Additional notes :

- Avoid informing the candidate of your decision, simply point out that our recruitment department will update them shortly.

> "I'll capture what we did here today in my notes, and someone from recruiting will get back to you with next steps as soon as they're able."

/label ~module ~"interview training"
/label ~"Module::Support Hiring - Stage2 Technical Interview Training"
