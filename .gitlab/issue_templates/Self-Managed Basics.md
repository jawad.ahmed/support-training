---
module-name: "Self-Managed Basics"
area: "Customer Service"
maintainers:
  - TBD
---

## Introduction

This checklist provides Support Engineers with the basics of answering Self-managed product-related tickets.

**Prerequisite**

- Installation & Administration Basics (<a href="https://gitlab.com/gitlab-com/support/support-training/-/issues/new?issuable_template=Gitlab%20Installation%20and%20Administration%20Basics" target="_blank">create new issue</a>)

**Goals of this checklist**

At the end of this module, you should be able to:

- answer self-managed tickets, including asking for appropriate logs or other diagnostics.
- schedule and participate in calls with customers.

**General Timeline and Expectations**

- Read about our [Support Onboarding process](https://about.gitlab.com/handbook/support/training/); the page also shows you the modules under the Self Managed Support Basics Pathway.
- This issue should take you **2 weeks to complete**.

Reminders:

- **Public** issue: Don't include anything confidential.

### Stage 0: Create Your module

1. [ ] Create an issue using this template by making the Issue Title: Self-Managed Basics - your name
1. [ ] Add yourself (and your trainer) as the assignees.
1. [ ] Set milestones, if applicable, and a due date to help motivate yourself!
1. [ ] Submit an [Individual or Bulk Access Request](https://about.gitlab.com/handbook/business-technology/team-member-enablement/onboarding-access-requests/access-requests/#individual-or-bulk-access-request) to be added to the `support-selfmanaged` Slack **Group** (not CHANNEL). This can only be done by a Slack Admin in our IT department. In the `System name` area, specify as follows:

```
- [ ] System name: Slack Group support-selfmanaged
    - Justification for this access: I am completing the Self-managed Learning Module.
```

Consider using the Time Tracking functionality so that the estimated length for the module can be refined.

### Stage 1: Gathering Diagnostics

- [ ] **Done with Stage 1**

**Goal** Understand the gathering of diagnostics for GitLab instances

1. [ ] Learn about the GitLab checks that are available
    1. [ ] [Environment Information and maintenance checks](https://docs.gitlab.com/ee/administration/raketasks/maintenance.html)
    1. [ ] [GitLab check](https://docs.gitlab.com/ee/administration/raketasks/check.html)
    1. [ ] Omnibus commands
        1. [ ] [Status](https://gitlab.com/gitlab-org/omnibus-gitlab/-/tree/master/doc/maintenance#get-service-status)
        1. [ ] [Starting and stopping services](https://gitlab.com/gitlab-org/omnibus-gitlab/blob/master/doc/maintenance/#starting-and-stopping)
        1. [ ] [Starting a rails console](https://docs.gitlab.com/ee/administration/operations/rails_console.html#starting-a-rails-console-session)

1. [ ] Learn about additional Support Tools
    1. [ ] [GitLab SOS](https://gitlab.com/gitlab-com/support/toolbox/gitlabsos)
        1. [GreenHat](https://gitlab.com/gitlab-com/support/toolbox/greenhat)
        1. [SOS Analyzer](https://gitlab.com/gitlab-com/support/toolbox/sos-analyzer)
        1. [SOS For K8S](https://gitlab.com/gitlab-com/support/toolbox/kubesos)
    1. [ ] [Fast-Stats](https://gitlab.com/gitlab-com/support/toolbox/fast-stats)
    1. [ ] [strace-parser](https://gitlab.com/gitlab-com/support/toolbox/strace-parser)

### Stage 2: Customer Calls

- [ ] **Done with Stage 2**

Look at the `GitLab Support` Google Calendar to find customer calls to observe. Contact the person leading the call to check if it is okay for you to jump in on the call, and if they could stay on with you for a few minutes after the call, so you can ask them a few questions about the things you didn't understand. Also, ask them to ask you a few questions to make sure you understand the points they want to highlight from the call.

1. [ ] Read about [customer calls](https://about.gitlab.com/handbook/support/workflows/customer_calls.html).
    - Note: Most of our calls will be via Zoom, but some customers cannot use Zoom. We have [Cisco WebEx](https://about.gitlab.com/handbook/support/workflows/customer_calls.html#webex) as an alternative.
1. Start arranging to pair on calls with other Support Engineers. Aim for 3-5.
    1. [ ] call with support_team_member_name - ticket_link
    1. [ ] call with support_team_member_name - ticket_link
    1. [ ] call with support_team_member_name - ticket_link
1. Reverse Shadowing _(**You** lead calls with a GitLab Support Engineer giving you feedback and support)_
    - Once you start needing to schedule support calls with customers, invite other support team members to shadow your calls.
    - Aim for 2-5 of these reverse shadowing calls; keep going if you still feel uncomfortable, or if the topic looks challenging.
    - Good places to find team members to shadow your calls include asking your onboarding buddy, asking in the self-managed slack channel, or look in the [Support Team Knowledge Areas](https://about.gitlab.com/handbook/support/workflows/knowledge_areas.html) page of the handbook for people who may know more about the topic of the call.
    1. [ ] call with support_shadower_name - ticket_link
    1. [ ] call with support_shadower_name - ticket_link
    1. [ ] call with support_shadower_name - ticket_link
1. [ ] Learn about how [customers can request upgrade assistance](https://about.gitlab.com/handbook/support/workflows/upgrade-assistance.html).
1. [ ] In rare cases, you may need to help a customer [patch an instance](https://about.gitlab.com/handbook/support/workflows/patching_an_instance.html).

**Setup Calendly**

1. [ ] Read our [Calendly page](https://about.gitlab.com/handbook/support/workflows/calendly.html) and change it from the GitLab setup to how Support use it, including:
    1. [ ] Set up Zoom integration on your Calendly account by going to [calendly integrations](https://calendly.com/integrations) and following the instructions for Zoom (make sure to [update the calendly event's location to use "Zoom"](https://help.calendly.com/hc/en-us/articles/360010008093-Zoom))
    1. [ ] Set the title of your personal calendly customer event to include 'support' so the zap works.
    1. [ ] Add a required question to your personal calendly customer event to ask the customer to enter the GitLab Support ticket number.
    1. [ ] Set up your [availability schedules](https://about.gitlab.com/handbook/support/workflows/calendly.html#availability-schedule-setup).
    1. [ ] Install the browser plugin.

## Penultimate stage: Review

Any updates or improvements needed? If there are any dead links, out of date or inaccurate content, missing content whether in this module or in other documentation, list it below as tasks for yourself! Once ready, have a [maintainer or manager review](https://gitlab.com/gitlab-com/support/support-training#guideline-to-update-support-training-module).

## Final Stage:

1. [ ] Have your buddy or trainer review your progress in this module.
1. [ ] At your next 1:1, review this the module with your manager and agree on completion status.
1. [ ] Discuss with your manager which module(s) to do next. Suggestions: [Omnibus module](https://gitlab.com/gitlab-com/support/support-training/-/blob/master/.gitlab/issue_templates/GitLab%20Omnibus.md), [Continuous Integration module](https://gitlab.com/gitlab-com/support/support-training/-/blob/master/.gitlab/issue_templates/Continuous%20Integration.md).
1. [ ] Submit an MR to update the [Support Team yaml file](https://gitlab.com/gitlab-com/support/team/-/blob/master/data/support-team.yaml) with this training module's topic under your list of completed `modules`. Discuss with your manager what the percentage should be.
1. [ ] Submit an MR to update `modules` and `knowledge_areas` in the [Support Team yaml file](https://gitlab.com/gitlab-com/support/team/-/blob/master/data/support-team.yaml) with this training module's topic. You will now be listed as an expert in this topic on [Skills by Person page](https://gitlab-com.gitlab.io/support/team/skills-by-person.html).


/label ~module
/label ~"Module::Self-Managed Basics"
