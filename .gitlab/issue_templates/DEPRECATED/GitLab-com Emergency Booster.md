---
module-name: "GitLab-com Emergency Booster"
area: "Customer Service"
maintainers:
  - lyle
---

## Customer Emergency On-Call Booster

**Goal** Be familiar with the additional information that will be helpful when handling GitLab.com emergencies that was not covered in your Customer Emergency Module.

## Stage 1: Incident Basics:

*Purpose*: After completing this section, you will understand the Incident Management process on GitLab.com so that you'll know who is doing what during an incident.

1. [ ] Familiarize yourself with the [Emergency Runbook](https://gitlab.com/gitlab-com/support/emergency-runbook) that you can use during an emergency.
1. [ ] Read the [Incident Management](https://about.gitlab.com/handbook/engineering/infrastructure/incident-management/) page from the Infrastructure section of the GitLab handbook to understand how to collaborate with the Site Reliability Engineers on-call for GitLab.com emergencies. Take special note of:
   + [What an incident is](https://about.gitlab.com/handbook/engineering/infrastructure/incident-management/#incident-management).
   + What [roles](https://about.gitlab.com/handbook/engineering/infrastructure/incident-management/#roles-and-responsibilities) are assumed during an incident.
   + The [definitions](https://about.gitlab.com/handbook/engineering/infrastructure/incident-management/#status) of the different state of operations that the GitLab.com platform may be in during an incident


## Stage 2: Reference Architecture, Monitoring and Logs

*Purpose*: After completing this section, you will have an understanding of the architecture, monitoring and logging of GitLab.com so that you can triage customer issues effectively.

1. [ ] Familiarize yourself with the the [Reference Architectures](https://docs.gitlab.com/ee/administration/reference_architectures/index.html).
1. [ ] Read through the [Production Architecture](https://about.gitlab.com/handbook/engineering/infrastructure/production/architecture/) document to gain a basic understanding of the infrastructural layout of GitLab.com, note the similarities (and differences) with the self-managed reference architectures.
1. [ ] Read about the [Monitoring of GitLab.com](https://about.gitlab.com/handbook/engineering/monitoring/) to understand how our infrastructure team monitors the performance of GitLab.com.
1. [ ] Read about which [critical dashboards](https://about.gitlab.com/handbook/engineering/infrastructure/incident-management/#definition-of-outage-vs-degraded-vs-disruption) show if GitLab.com is experiencing an incident and then bookmark the following ones:
    + [Key Service Metrics Dashboard](https://dashboards.gitlab.net/d/general-service/general-service-platform-metrics?orgId=1)
    + [Triage](https://dashboards.gitlab.net/d/RZmbBr7mk/gitlab-triage?orgId=1&refresh=30s)
    + [General Triage](https://dashboards.gitlab.net/d/general-triage/platform-triage?orgId=1)
1. Learn about finding errors in logs on GitLab.com - Note: Kibana and Sentry sign in use the dev account.
    1. [ ] [Using Kibana](https://about.gitlab.com/handbook/support/workflows/kibana.html).
        - Note: Typical usage is `rails` and `sidekiq` logs. `HAproxy` is only available to infra team. Ask in the #production Slack channel if necessary.
    1. [ ] [Searching Kibana, Sentry, and filing GitLab issues from Sentry](https://about.gitlab.com/handbook/support/workflows/500_errors.html).
    1. [ ] As an exercise, visit a non-existing page on GitLab.com, such as [gitlab.com/gitlab-com/doesntexist](https://gitlab.com/gitlab-com/doesntexist). Search Kibana for the 404 error using your username. Add a screenshot of the relevant portion of the error you find in Kibana as a comment to this issue.
    1. [ ] Search Sentry using the `correlation_id`. You may not find anything and the search is not reliable, but take a screenshot of your search and results and add it as a comment to this issue anyway.

## Stage 3: Getting Prepared

*Purpose*: After completing this section, you will have all the links, bookmarks and channels needed to be aware of on-going incidents in the event of an emergency page.

Your workspace should be configured to be as prepared as possible for an incident. This means having essential issue trackers bookmarked, joining incident management related Slack channels, and being aware of where reports of issues from end-users will surface.

### Issue Trackers

1. [ ] Bookmark these issues trackers.
    + [Production](https://gitlab.com/gitlab-com/gl-infra/production/issues)
    + [CMOC Handover](https://gitlab.com/gitlab-com/support/dotcom/cmoc-handover/issues)
    + [Emergency Runbook](https://gitlab.com/gitlab-com/support/emergency-runbook)

### Slack Channels

1. [ ] Join these Slack channels
    + [#production](https://gitlab.slack.com/messages/C101F3796)
    + [#incident-management](https://gitlab.slack.com/messages/CB7P5CJS1)

## Stage 4: GitLab.com Access

*Purpose*: After completing this section, you will have sufficient access to view customer information and verify correct behavior if a patch is developed during the course of an incident.

### GitLab.com Admin Access

- [ ] Complete [GitLab.com Admin access Training Module](.gitlab/issue_templates/GitLab-com Admin.md)


### GitLab.com Staging Access
You may use staging.gitlab.com in the process of verifying a patch generated in the scope of an emergency before pushing it out to production.
You don't need access to any special account type, a normal user account should be sufficient.

1. [ ] Attempt to sign in from [https://staging.gitlab.com/users/sign_in](https://staging.gitlab.com/users/sign_in) using your GitLab username and password, or OAuth from Google. If it works, there's no need to continue!
1. [ ] Open a [Individual Access request](https://gitlab.com/gitlab-com/team-member-epics/access-requests/-/issues/new?issuable_template=Individual_Bulk_Access_Request) for staging.gitlab.com

/milestone %".com Emergency Roll Out"
/label ~oncall-training
/label ~"Module::GitLab-com Emergency Booster"
