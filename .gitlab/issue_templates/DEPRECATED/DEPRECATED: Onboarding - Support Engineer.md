**NOTE:** This issue is [DEPRECATED](https://gitlab.com/gitlab-com/support/support-training/-/merge_requests/375) - for onboarding new support team members, please use the [`New-Support-Team-Member-Start-Here`](https://gitlab.com/gitlab-com/support/support-training/issues/new?issuable_template=New-Support-Team-Member-Start-Here) issue template instead.

Welcome to the Support team!

> **As you work through this onboarding issue, please remember that this issue is [non-confidential and is public](https://about.gitlab.com/handbook/values/#transparency). With that said, please do not add any confidential data such as customer names, logs, etc. to this issue. Linking to the appropriate [Zendesk ticket](https://about.gitlab.com/handbook/support/workflows/working_with_security.html#general-guidelines-1) is OK!**

**General timeline and expectations**: We believe that people often learn best and fastest by diving into (relatively easy) tickets and learning by doing. However, this has to be balanced with making time to learn some of the tougher topics. You should start tackling **stages 0, 1, 2, 3, and 4** as early as your first week, though you are not likely to complete all items for at least several weeks. The expectation is therefore that you are sufficiently advanced in your onboarding by the end of your first week that you can answer 2-5 "simple" customer tickets.

Over the following weeks, your manager will set targets for the number and difficulty of tickets to be tackled, you should always save about 2-3 hours per day to spend on working through this onboarding issue. Some days it will be less, some days it will be more, depending on your working style and ticket load; however an average of 2-3 hours per day should allow you to complete everything within about four weeks.

**Goal of this entire checklist:** Ensure team member has everything they need to get started.

## Manager Tasks pre-Stage 0

1. [ ] Tag onboarding buddy on this issue (below).
1. [ ] Add team member to the `GitLab Support` Google Cloud Project with `Support Permissions`.

## Your Onboarding Buddy: `[tag buddy]`

### Stage 0: Complete general onboarding to have all necessary accounts and permissions

- [ ] **Done with Stage 0**

_Typically completed within the first week_

1. [ ] General Onboarding Checklist: this should have been created for you on [the People Ops Employment issue tracker](https://gitlab.com/gitlab-com/team-member-epics/employment)
   when you were hired.
   1. [ ] Finish every item on that list starting with `New team member:` until you
      are waiting for someone to answer a question or do something before you can continue that list.
   1. [ ] Slack - join `#support_team-chat`, `#support_gitlab-com`, `#support_self-managed`, `#support_managers`
   1. [ ] Start Stage 1 here, as well as the first steps of Stage 2 through Stage 4.
   1. [ ] Check back daily on what was blocking you on your General Onboarding Checklist
      until that list is completely done, then focus on this one.

### Stage 1: Become familiar with git and GitLab basics

- [ ] **Done with Stage 1**

If you are already comfortable with using Git and GitLab and you are able to
retain a good amount of information by just watching or reading through, that is
okay. But if you see a topic that is completely new to you, stop the video and try
it out for yourself before continuing.

Go over these topics in [GitLab University](https://docs.gitlab.com/ee/university/):

1. [ ] Under the topic of [Version Control and Git](https://docs.gitlab.com/ee/university/#1-1-version-control-and-git)
   1. [ ] [About Version Control](https://docs.google.com/presentation/d/16sX7hUrCZyOFbpvnrAFrg6tVO5_yT98IgdAqOmXwBho/edit#slide=id.gd69537a19_0_14)
   1. [ ] [Try Git](https://www.katacoda.com/courses/git)
   1. [ ] Explore [Git internals](https://git-scm.com/book/en/v2/Git-Internals-Plumbing-and-Porcelain)
     and go back to it from time to time to learn more about how Git works
1. [ ] Under the topic of [GitLab Basics](https://docs.gitlab.com/ee/university/#12-gitlab-basics)
   1. [ ] All the [GitLab Basics](http://docs.gitlab.com/ee/gitlab-basics/README.html)
      that you don't feel comfortable with. If you get stuck, see the linked videos
      under GitLab Basics in GitLab University
   1. [ ] [GitLab Flow](https://www.youtube.com/watch?v=UGotqAUACZA)
   1. [ ] Read about [GitLab releases](https://about.gitlab.com/handbook/engineering/releases/#overview-and-terminology)
   1. [ ] Take a look at how the different GitLab versions [compare](https://about.gitlab.com/features/)
1. [ ] Any of these that you don't feel comfortable with in the [user training](https://gitlab.com/gitlab-org/gitlab-ee/tree/master/doc/university/training/topics)
   we use for our customers.
   1. [ ] `env_setup.md`
   1. [ ] `feature_branching.md`
   1. [ ] `explore_gitlab.md`
   1. [ ] `stash.md`
   1. [ ] `git_log.md`
   1. [ ] For the rest of the topics in `user training`, just do a quick read over
      the file names so you start remembering where to find them.
1. [ ] Get familiar with the services GitLab offers
   1. [ ] The differences between GitLab.com (SaaS) [Free, Bronze, Silver, Gold](https://about.gitlab.com/pricing/#gitlab-com) and self-managed [Core, Starter, Premium, Ultimate](https://about.gitlab.com/pricing/#self-managed)
1. [ ] Get familiar with the [different teams in-charge of every stage in the DevOps cycle](https://about.gitlab.com/handbook/product/categories/#devops-stages) and what they are responsible for. This will assist you in adding the right labels when creating issues and escalating in the right Slack channel.

### Stage 2. Installation and Administration basics.

- [ ] **Done with Stage 2**

**Goals**

- Get your development machine set up.
- Familiarize yourself with the codebase.
- Be prepared to reproduce issues that our users encounter.
- Be comfortable with the different installation options of GitLab.
- Have an installation available for reproducing customer bug reports.

**Set up your development machine**

1. [ ] Install the [GDK](https://gitlab.com/gitlab-org/gitlab-development-kit). If you like Docker, especially if you're running Linux
    on your desktop, consider the [GitLab Compose Kit](https://gitlab.com/gitlab-org/gitlab-compose-kit).

**Installations**

You will keep one installation continually updated on Digital Ocean (managed through terraform),
just like many of our clients. You need to choose where you would like to test
other installations. _(TODO: We need to list some benefits of each choice here.)_

1. [ ] Use terraform to create a new test instance.
    1. [ ] Clone the dev-resources project.
    1. [ ] Read up on [how to create a new instance](https://gitlab.com/gitlab-com/dev-resources/blob/master/dev-resources/README.md).
    1. [ ] Create a new file and name it `<your-full-name>.tf` in a new branch. You
       can copy another team member's file and modify it or you can create your own
       from scratch. If you have any issues or questions ask in the `#support_self-managed` channel.
    1. [ ] After you've created it, open up a merge request and if the tests pass then merge it.
1. [ ] Set up your [test environments](https://about.gitlab.com/handbook/support/workflows/test_env.html).
1. [ ] Choose between Local VM's or Digital Ocean for your preferred test environment, and note it in a comment below.
1. [ ] Perform each of the following [Installation Methods](https://about.gitlab.com/installation/)
   on the test environment you chose above:
   1. [ ] Install via [Omnibus](https://gitlab.com/gitlab-org/omnibus-gitlab/)
   1. [ ] Populate with some test data: User account, Project, Issue
        - [Utilize the GitLab API to seed your test instance](https://gitlab.com/gitlab-com/support/support-training/blob/master/seed-data-api.md)
   1. [ ] Backup using our [Backup rake task](http://docs.gitlab.com/ee/raketasks/backup_restore.html#create-a-backup-of-the-gitlab-system)
   1. [ ] Install via [Docker](https://docs.gitlab.com/ee/install/docker.html) on your local machine.
   1. [ ] (Optional for Application Support) Restore a backup to your Docker VM using our [Restore rake task](http://docs.gitlab.com/ee/raketasks/backup_restore.html#restore-a-previously-created-backup)
   1. [ ] (Optional for Application Support) Install GitLab from [Source](https://docs.gitlab.com/ee/install/installation.html).
      Installation from source is not common but will give you a greater understanding
      of the components that we employ and how everything fits together.
        - [Have a copy of GitLab source code available offline for reference](https://gitlab.com/gitlab-com/support/support-team-meta/issues/1900#2-have-a-copy-of-gitlab-source-code-available-offline-for-reference)
1. [ ] With your manager or another Support team member, manually generate 3 'self managed' licenses at https://license.gitlab.com/ - one for each of Starter, Premium, and Ultimate. Suggest around 100 users with 5 year expiry. Keep the licenses securely in 1Password. Use the licenses on your test instance when replicating customer issues. By having the correct license you will ensure feature parity with what the customer is seeing.
1. The services below are part of the many services used by our customers. Installing these services and learning how they work with GitLab will prepare you for Stage 3 and give you a solid foundation for when you're ready to resolve tickets on your own.
    1. [ ] [GitLab Runner](https://docs.gitlab.com/runner/) and connect it to your GitLab instance.
    1. [ ] (Optional for Application Support) [Elastic Search](https://www.elastic.co/guide/en/elasticsearch/reference/current/deb.html)
        1. [ ] Install on a VM or on a separate instance on Terraform
            * The [steps outlined on Digital Ocean](https://www.digitalocean.com/community/tutorials/how-to-install-and-configure-elasticsearch-on-ubuntu-18-04) should help you configure your instance's IP.
            * Set `cluster.name` and `node.name` and use the latter in `cluster.initial_master_nodes` to enable ["single node" mode](https://www.elastic.co/guide/en/elasticsearch/reference/7.6/add-elasticsearch-nodes.html).
        1. [ ] [Connect Elastic Search to your GitLab instance and index your repositories](https://docs.gitlab.com/ee/integration/elasticsearch.html). You need to apply a license to your GitLab instance for the integration settings to appear if you haven't already.
    1. [ ] Connect your GitLab instance to a new cluster via the [Kubernetes clusters](https://docs.gitlab.com/ee/user/project/clusters/) integration and install a Runner in the cluster:
        1. [ ] Login to https://console.cloud.google.com/ with your work email.
        1. [ ] Create a cluster under the [project `gitlab-support`](https://console.cloud.google.com/kubernetes/list?project=support-testing-168620).
           Start small to [cut costs to about one tenth](https://about.gitlab.com/handbook/values/#spend-company-money-like-its-your-own) of the default cluster config:
              - Set `Location type` to a single zone (instead of a region)
              - Set `Node pool` `Size` to 1
              - Set the `Nodes`' `Machine type` to a `smaller` one than the default (recommend `e2-medium` for 2vCPU and 4GB RAM)
              - Review the [general tips for cost optimization](https://about.gitlab.com/handbook/support/workflows/cost-optimizations-with-cloud-instances.html#general-tips-for-cost-optimization)
        1. [ ] You will also need to install the [`gcloud SDK`](https://cloud.google.com/sdk/gcloud/reference/init).
        1. [ ] [Add](https://docs.gitlab.com/ee/user/project/clusters/add_remove_clusters.html) the Kubernetes cluster to GitLab.
        1. [ ] Create a project on your self-managed instance, and then create a file named `.gitlab-ci.yml`.
        1. [ ] Use [the yaml docs](https://docs.gitlab.com/ee/ci/yaml/#parameter-details) as a resource to build a basic script.
        1. [ ] Make sure your project has access to your Kubernetes cluster under Operations -> Kubernetes.
        1. [ ] Make sure your project has registered runners under your project's Settings -> CI/CD -> Runners.
        1. [ ] Pat yourself on the back; you're now ready to reproduce some of our common customer tickets.
        1. [ ] Remove the cluster when you're done!
1. [ ] Watch the [GitLab Debugging Techniques: A Support Engineering Perspective](https://www.youtube.com/watch?v=9W6QnpYewik) video on GitLab Unfiltered (YouTube). This dives into some common issues that customers might run into.
1. [ ] In the future, you may need other applications for testing. Don't set these up now, but check out the [infrastructure for troubleshooting section](https://about.gitlab.com/handbook/support/workflows/#Infrastructure%20for%20troubleshooting) of workflows so you know what's available.
1. [ ] Where to get help on Slack:
   - #gdk
   - #questions for general GitLab questions
   - #support_team_chat for support specific setup
1. [ ] You can check this box but this one never stops as long as you are a Support Engineer for GitLab:
       Keep this installation up-to-date as patch and version releases become available, just like our customers would.

### Stage 3. Customer Interaction through Zendesk

- [ ] **Done with Stage 3**

**Goals**

- Have a good understanding of ticket flow in Zendesk and how to interact with our various channels.
- See some common issues that our customers face and how to resolve them.

**Basic reference**

1. [ ] Get familiar with what [support workflows](https://about.gitlab.com/handbook/support/workflows/) exist. We recommend not to read through the workflows so much as know what is available for reference later.
1. [ ] Get familiar with our [Customer facing Support page](https://about.gitlab.com/support/) and [Statement of Support](https://about.gitlab.com/support/statement-of-support.html)

**Zendesk Basics module**

Zendesk is our Support Center and our main communication line with our customers.
We communicate with customers through several other channels too, see the support
handbook for the full list. To get familiar with Zendesk, create an issue based on the template [Module - Zendesk Basics](https://gitlab.com/gitlab-com/support/support-training/-/blob/master/.gitlab/issue_templates/Module%20-%20Zendesk%20Basics.md) and
go through the module.

1. [ ] Zendesk Basics module completed
1. [ ] Consider installing the [Zendesk Download Router](https://gitlab.com/gitlab-com/support/toolbox/zd-dl-router), which will sort downloaded Zendesk ticket attachments into their own folders. This is particularly useful for Solutions focused Support engineers.

**Learn about the Support process**

While you can start doing pairings before starting the later stages, it is important that you work through and finish Stage 1-4 as you do pairing sessions so that you can connect what you're learning with what's happening during pairing sessions.

1. [ ] Perform 15 one-hour pairing sessions with Support Engineers. Focus on team members with the same focus (Application/Solutions), but at least 3 pairings should be with team members in the other focus. Also consider people outside of your immediate region to avoid silos. Create a Google Calendar event, inviting
   that person. If you're unsure of times due to timezones, feel free to send them a message in Slack first.
   When it's time for the pairing session, create a new [Support Pairing project Issue](https://gitlab.com/gitlab-com/support/support-pairing)
   and use the appropriate template for the call.
   At least five sessions should be where you share your screen while you answer tickets on Zendesk, and including at least one with your Onboarding Buddy, where they give you feedback and answer your questions. The goal of the call with your onboarding Buddy is for them to pass on tactical knowledge about the ticket handling process. Repeat this step a few times if you find it useful.
   1. [ ] call with ___; issue link: ___
   1. [ ] call with ___; issue link: ___
   1. [ ] call with ___; issue link: ___
   1. [ ] call with ___; issue link: ___
   1. [ ] call with ___; issue link: ___
   1. [ ] call with ___; issue link: ___
   1. [ ] call with ___; issue link: ___
   1. [ ] call with ___; issue link: ___
   1. [ ] call with ___; issue link: ___
   1. [ ] call with ___; issue link: ___
   1. [ ] call with ___; issue link: ___
   1. [ ] call with ___; issue link: ___
   1. [ ] call with ___; issue link: ___
   1. [ ] call with ___; issue link: ___
   1. [ ] call with ___; issue link: ___
1. [ ] Dive into our Zendesk support process by reading how to [handle tickets](https://about.gitlab.com/handbook/support/workflows/working-on-tickets.html).
   - With special attention to the [Zendesk SLA settings and Breach Alerts](https://about.gitlab.com/handbook/support/workflows/working-on-tickets.html#understanding-slas).
1. [ ] Learn about our [Support Modes of Work](https://about.gitlab.com/handbook/support/workflows/meeting-service-level-objectives.html).
   - Here you will learn about our rotating roles so that people know what to do and have variation in their work.
1. [ ] Start getting real world experience by handling real tickets, all the while gaining further experience with GitLab.
   1. [ ] First, learn about our [Support Channels](https://about.gitlab.com/handbook/support/channels).
   1. [ ] Proceed on to [Support Tickets for customers](https://about.gitlab.com/handbook/support/channels/#support-tickets-for-customers-on-paid-plans) and read over [how to respond to tickets](https://about.gitlab.com/handbook/support/workflows/how-to-respond-to-tickets.html).
      - Here you will find tickets from both paid customers and free/trial users.
      - Tickets are extremely varied.
      - You should be prepared for basic tickets given the knowledge gained from the previous steps of your training.
      - Tickets in Zendesk contain a wealth of knowledge from past interactions. Often times, the issue has occurred at least once and a solution has been provided, or an issue created on the customer's behalf. Reading through past tickets can aide in the learning process, getting you up to speed quicker.
    1. [ ] Bookmark the [Zendesk Support search reference](https://support.zendesk.com/hc/)
1. [ ] Check out your colleagues' responses.
   1. [ ] Hop on to the `#feed_zd-self-managed` or `#feed_zd-gitlab-com` channel in Slack and see the tickets as they
      come in and are updated.
   1. [ ] Read through about 20 old tickets that your colleagues have worked on and their responses.
1. Get to know the GitLab [Support Bot](https://gitlab.com/gitlab-com/support/support-ops/gitlab-support-bot/tree/master).
   1. [ ] Any Support Slack channel, type "sb sh" (self-manageD) or "sb dc" (GitLab.com). This will return a list of the number of active tickets by type.

**Learn how to look up customer account details**

Make sure that you can access all three resources below:

1. [ ] [SFDC aka Salesforce](https://gitlab.my.salesforce.com/home/home.jsp): can be used to check various account details. Check [Salesforce page](https://about.gitlab.com/handbook/support/workflows/looking_up_customer_account_details.html#within-salesforce)
  to learn how to access and use it.
1. [ ] [License app](https://license.gitlab.com/): can be used to search for self-managed licenses and generate them.
  You should be able to access it using your dev.gitlab.org account.
1. [ ] [Customer portal](https://customers.gitlab.com/admin/customer): can be used to search for information about all customers. Used by customers themselves.
  Check [customers.gitlab.com page](https://about.gitlab.com/handbook/support/workflows/looking_up_customer_account_details.html#within-customersgitlabcom)
  to learn how to access and use it.

**Customer Feedback**

1. [ ] Read over how to handle [feedback and complaints](https://about.gitlab.com/handbook/support/workflows/feedbacks_and_complaints.html).

**Learn about the Escalation process for Issues**

Some tickets need specific knowledge or a deep understanding of a particular component
and will need to be escalated to a Senior Support Engineer or a Developer.

1. [ ] Read about [creating and escalating issues](https://about.gitlab.com/handbook/support/workflows/working-with-issues.html)
   including how issues are priotized.
1. [ ] Take a look at the [Team page](https://about.gitlab.com/team/)
   to find the resident experts in their fields and the [knowledge areas page](https://about.gitlab.com/handbook/support/workflows/knowledge_areas.html) for further topics where team members are keen on helping with specific areas.
1. [ ] Add yourself to the [knowledge areas page](https://about.gitlab.com/handbook/support/workflows/knowledge_areas.html) for topics you may know already from previous experience.

**Learn about raising issues and fielding feature proposals**

1. [ ] Understand what's in the pipeline at GitLab as well as proposed features: [Direction Page](https://about.gitlab.com/direction/).
1. [ ] Practice searching issues and filtering using [labels](https://gitlab.com/groups/gitlab-org/-/labels)
   to find existing feature proposals and bugs.
1. [ ] If raising a new issue always provide a relevant label and a link to the relevant ticket in Zendesk.
1. [ ] Add [customer labels](https://about.gitlab.com/handbook/support/workflows/working-with-issues.html#adding-labels)
   for those issues relevant to our subscribers.
1. [ ] Take a look at the [existing issue templates](https://gitlab.com/gitlab-org/gitlab/tree/master/.gitlab/issue_templates)
   to see what is expected (look at the comments in the markup for details.)
1. [ ] Raise issues for bugs in a manner that would make the issue easily reproducible.
   A Developer or a contributor may work on your issue.

**Making improvements**

1. [ ] Learn how to [automatically file a documentation improvement GitLab issue through ZenDesk](https://about.gitlab.com/handbook/support/workflows/improving-documentation.html).
1. If you're interested in making a contribution to the code, please see the [GitLab development guidelines](https://docs.gitlab.com/ee/development/).

**Requests for help**

Read over these resources:

1. [ ] To ask for help, post in the appropriate Slack channel and add a link as an internal note in the ticket, if applicable.
   - #questions for general GitLab questions
   - #support_gitlab-com or #support_self-managed for ticket (or issue) specific questions
   - Appropriate #s_ (stage) or #g_ (group) channel. Broken up according to the [product categories page](https://about.gitlab.com/handbook/product/categories/#devops-stages).
1. [ ] When you get no response or need someone more urgently, you can ping the appropriate [support counterpart](https://about.gitlab.com/handbook/support/#support-stable-counterparts) or someone with [knowledge in the area](https://about.gitlab.com/handbook/support/workflows/knowledge_areas.html).
1. [ ] If someone asks you for help, whether directed at the team or you individually, consider these [Slack reactions tips](https://about.gitlab.com/handbook/support/workflows/communication-tips.html#slack-reactions).
1. [ ] If you need to bring something to the team's attention but don't want to ping people, consider using [mustread Slack app](https://about.gitlab.com/handbook/support/workflows/how-to-use-mustread.html)

#### Congratulations. You now have your Zendesk Wings!

From now on you can spend most of your work time answering tickets in Zendesk. Try
to set aside 2 hours per day to make it through the rest of your onboarding. Never
hesitate to ask as many questions as you can think of in the appropriate Slack channel.

### Stage 4: Working on tickets

- [ ] **Done with Stage 4**

All team members should be able to answer basic questions for both products, GitLab.com (SaaS) and Self-managed. Where more time is spent depends on a Support Engineer's focus. Application focused team members are more likely to split time between GitLab.com and Self-managed tickets, whereas Solutions focused team members will typically answer more tickets in Self-managed.

1. [ ] Open a module on [GitLab.com Basics](https://gitlab.com/gitlab-com/support/support-training/issues/new?issuable_template=Module%20-%20GitLab.com%20Basics).
1. [ ] Open a module on [Self-managed Basics](https://gitlab.com/gitlab-com/support/support-training/issues/new?issuable_template=Module%20-%20Self-managed%20Basics).
1. [ ] With manager guidance: Open a module on [License and Renewals tickets (Growth)](https://gitlab.com/gitlab-com/support/support-training/issues/new?issuable_template=Module%20-%20Growth).

### Stage 5: On-call

- [ ] **Done with Stage 5**

Check with your manager which one to open.

1. [ ] Open an appropriate on-call module:
   - [Module - GitLab.com CMOC](https://gitlab.com/gitlab-com/support/support-training/issues/new?issuable_template=Module%20-%20GitLab.com%20CMOC)
   - [Module - Customer Emergencies](https://gitlab.com/gitlab-com/support/support-training/-/issues/new?issuable_template=Module%20-%20Customer%20Emergencies)

### Final Stage: Feedback and Documentation

- [ ] **Done with Final Stage**

Keeping our documentation and workflows up to date will ensure that all Support team members will be able to access and learn from the best practices of the past.
Giving feedback about your onboarding experience will ensure that this document is always
up to date and those coming after you will have an easier time coming in.

1. [ ] Schedule a call with your manager to discuss your onboarding issue or integrate this into your 1:1. Answer the following questions:
   - What things were helpful?
   - What things did you wish existed in your onboarding, but did not?
   - Which areas do you not feel confident in yet?
   - Which areas do you feel strong in?
   - Discuss which areas of onboarding need to be updated or created. Then update / create three of them:
      1. [ ] _________
      1. [ ] _________
      1. [ ] _________
1. [ ] Make an update to one or more onboarding or module template to make it better and link them below.
The file is located in an issue template in the ['support-training` repository](https://gitlab.com/gitlab-com/support/support-training/blob/master/.gitlab/issue_templates).
   1. _________

### Optional: Advanced GitLab Topics

Discuss with your manager if you should stop here and close your issue
or continue. Also, discuss which of the advanced topics should be followed. Do
not do all of them as they might not be relevant to what customers need
right now and can be a significant time investment.

These are some of GitLab's more advanced features. You can make use of
GitLab.com to understand the features from an end-user perspective and then use
your own instance to understand setup and configuration of the feature from an
Administrative perspective.

- [ ] Set up and try [Git LFS](https://docs.gitlab.com/ee/topics/git/lfs/index.html)
- [ ] Get to know the [GitLab API](https://docs.gitlab.com/ee/api/README.html), its capabilities and shortcomings.
- [ ] Create your first [GitLab Page](https://docs.gitlab.com/ee/administration/pages/index.html)
- [ ] Get to know [GitLab through the Rails Console](https://docs.gitlab.com/ee/administration/troubleshooting/navigating_gitlab_via_rails_console.html)
- [ ] Set up [GitLab CI](https://docs.gitlab.com/ee/ci/quick_start/README.html)
- [ ] Learn how to [migrate from SVN to Git](https://docs.gitlab.com/ee/user/project/import/svn.html)
- [ ] Set up [GitLab Geo](https://docs.gitlab.com/ee/administration/geo/replication/) with one read-only secondary node.
- [ ] [Advanced GitLab debugging and log collection](https://gitlab.com/gitlab-com/support/support-team-meta/-/issues/1900#5-try-something-a-little-more-advanced)

/label ~onboarding
