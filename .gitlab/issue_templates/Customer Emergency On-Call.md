---
module-name: "Customer Emergency On-Call"
area: "Customer Service"
maintainers:
  - lyle
---

## Customer Emergency On-Call (CEOC)

_Typically to be completed within 8 weeks of joining._

**Goal** Be familiar with the responsibilities of being on-call for Customer Emergencies

## Stage 1: On-call Basics

- [ ] Done with Stage 1

1. [ ] Read about on-call duty:
   1. [ ] GitLab's [on-call guide](https://about.gitlab.com/handbook/on-call/). On that
   page, the [Customer Emergency On-Call Rotation Section](https://about.gitlab.com/handbook/on-call/#customer-emergency-on-call-rotation)
   is what applies to Support Engineers, **NOT** the Production Team section.
   1. [ ] [GitLab Support's On-Call Guide](https://about.gitlab.com/handbook/support/on-call/) contains even more information specific to handling emergencies in Support.
   1. [ ] OPTIONAL: Consider reading [Notes and reflections on Customer Emergency On-Call Rotation: week of 22 February 2021](https://gitlab.com/gitlab-com/support/support-team-meta/-/issues/3202) for notes on what a busy on-call week could look like.
1. [ ] Familiarize yourself with the [Emergency Runbook](https://gitlab.com/gitlab-com/support/emergency-runbook) that you can use during an emergency. Consider _bookmarking_ the runbook project.
1. [ ] Open and complete the [On-Call Basics training module](https://gitlab.com/gitlab-com/support/support-training/-/issues/new?issuable_template=On-Call%20Basics).

## Stage 2: Reference Architecture

- [ ] Done with Stage 2

1. [ ] Familiarize yourself with the [Reference Architectures](https://docs.gitlab.com/ee/administration/reference_architectures/index.html). Note the similarities (and differences) with the GitLab.com reference architectures.

## Stage 3: GitLab.com Admin Access

- [ ] Done with Stage 3

- [ ] Complete [GitLab.com Admin access Training Module](.gitlab/issue_templates/GitLab-com Admin.md)

## Stage 4: Getting Scheduled for On-Call

- [ ] Done with Stage 4

1. [ ] Notify your manager to add you to the PagerDuty Rotation.
   * [ ] Manager: add the Engineer to the end of the user list, with at least 6 weeks of lead time before their first on-call shift.
1. [ ] Schedule time with your trainer to have them show you how to respond to Customer Emergencies.
1. [ ] In the weeks leading up to your first on-call shift, shadow the on-call engineers who are handling customer emergencies.
   * [ ] Create an [Issue](https://gitlab.com/gitlab-com/support/support-ops/other-software/pagerduty/-/issues/new?issuable_template=Change%20members%20in%20a%20schedule) to have Support-ops add you to the Customer Emergencies Shadow rotation for your region/timezone in PagerDuty. Make sure to specify which layer applies to you.
   * [ ] ZD Ticket: ___
   * [ ] ZD Ticket: ___
   * [ ] ZD Ticket: ___
   * [ ] Repeat this as many times as you are able.
1. [ ] Have your onboarding buddy or an available team member reverse shadow you on a customer emergency call (*you will be leading the call*).
   * [ ] ZD Ticket: ___
1. [ ] After your first on-call shift, create an MR to add the "On-call champion!" moniker to your entry on the [GitLab team page](https://gitlab.com/-/ide/project/gitlab-com/www-gitlab-com/tree/master/-/data/team_members/person/):
     ```expertise: <li><a href="/handbook/on-call/">On-call</a> champion!</li>```
1. [ ] You may expense the [cost of your mobile phone *service*](https://about.gitlab.com/handbook/support/on-call/#mobile-phone-service-and-data-reimbursement) for any month that you are performing on-call duties.

/due in 8 weeks
/label ~oncall-training
/label ~"Module::Customer Emergency On-Call"
