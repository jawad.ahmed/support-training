---
module-name: "triaging-tickets"
area: "Customer Service"
maintainers:
  - TBD
---

## Overview

**Goal**: This training module is designed to help team members understand how to triage tickets.

*Length*: This module should take you **4 days** to complete.

**Objectives**: At the end of this module, you should be able to:
- Triage tickets and make sure they are in the right view
- Check ticket metadata and correct them as needed

**Prerequisites**: Complete the `Zendesk Basics` module prior to starting this module.

## Stage 0: Create and commit to the module

1. [ ] Create an issue using this template by making the Issue Title: <module title> - <your name>
1. [ ] Notify your manager to let them know you've started.
1. [ ] Optional: Set a milestone, if applicable, and a due date to help motivate yourself!

Consider using the Time Tracking functionality so that the estimated length for the module can be refined.

## Stage 1: Let's begin triaging!

**What do we mean by ticket triaging?** Triaging a ticket includes ensuring the ticket has the right form, Support Level and SLA associated with it.

The triage page is where you would start, and there's a flowchart to help visualize the process: [Triaging Tickets](https://about.gitlab.com/handbook/support/workflows/ticket_triage.html).


While a ticket has [many fields](https://gitlab.com/gitlab-com/support/support-ops/zendesk-ticket-forms-and-fields), initial triaging of any ticket starts with ensuring the 4 fields below are correct to make sure users are taken care of appropriately.

1. [ ] Ticket Form: The form chosen on a ticket will route the ticket to the right set of individuals who can assist the customer on it, and also make sure the right metadata is associated with the ticket. It is very important that all tickets have the right form chosen. Take a look at the current forms in ZenDesk and what each one is for: [Applying the Correct Form](https://about.gitlab.com/handbook/support/workflows/ticket_triage.html#applying-the-correct-form)
1. [ ] Problem Type: Once the correct form is applied, it is important to choose the `Problem Type` of the ticket. Problem types depend on the type of form chosen.
1. [ ] Priority: It is important to ensure that every ticket has the right Priority associated with it, as it determines the SLAs received on the ticket, and helps us prioritize work.
     1. Read about [Setting Ticket Priority](https://about.gitlab.com/handbook/support/workflows/setting_ticket_priority.html#setting-ticket-priority).
     1. Understand how to [Reset Ticket Priority](https://about.gitlab.com/handbook/support/workflows/setting_ticket_priority.html#resetting-ticket-priority).

**Other Tickets you might see**

1. [ ] If you see a US Federal ticket in the general Support portal views, you can follow the instructions detailed here: [US Federal tickets in Global Support Portal](https://about.gitlab.com/handbook/support/workflows/ticket_triage.html#us-federal-tickets-in-global-support-portal).
1. [ ] You might also see non Support related requests in our views from time to time. Read about how to handle these here: [Other Requests](https://about.gitlab.com/handbook/support/workflows/ticket_triage.html#other-requests).
1. [ ] Occasionally, you might also see Spam tickets in the views. Read about how to handle these here: [Marking tickets as spam in Zendesk](https://about.gitlab.com/handbook/support/workflows/marking_tickets_as_spam.html); more details on the [alternate way here](https://support.zendesk.com/hc/en-us/articles/203691106-Marking-a-ticket-as-spam-and-suspending-the-requester).

## Stage 2: Now that it has the right form, I still want to make sure it has the right SLA and is in the correct View!


**SLAs and Support Levels**

SLAs are determined by the [GitLab Support Service Levels](https://about.gitlab.com/support/#gitlab-support-service-levels). Every ticket should have the right SLA associated with it to ensure the right attention is given to it. Normally, Zendesk assigns the appropriate SLA to each ticket based on the support level that is assigned to its associated organization. There might be cases where this doesn't happen, read on to find out more!

1. [ ] Read about the Service Levels that do not have SLAs: [Appropriate SLA by plan](https://about.gitlab.com/handbook/support/workflows/sla_and_views.html#appropriate-sla-by-plan)
1. [ ] Sometimes, an organization might be incorrectly marked as having `Expired` support level in Salesforce, but will be a currently paying customer. Read this section to understand the process to follow in such cases: [Handling customers with incorrect expired support](https://about.gitlab.com/handbook/support/workflows/sla_and_views.html#handling-customers-with-incorrect-expired-support)
   Note that this process has several actions to be performed simultaneously with the above, to make sure that the customer receives appropriate SLAs and attention on the ticket while we sort out the system issue:
   1. [ ] [Fixing tags for tickets with Expired organization](https://about.gitlab.com/handbook/support/workflows/sla_and_views.html#fixing-tags-for-tickets-with-expired-organization)
   1. [ ] [Verifying that the ticket now has the proper SLA applied](https://about.gitlab.com/handbook/support/workflows/sla_and_views.html#verifying-that-the-ticket-now-has-the-proper-sla-applied)
1. [ ] There may be cases where the organization exists in Zendesk, but its support level does not match the support level in Salesforce. You can follow [the same workflow as above](https://about.gitlab.com/handbook/support/workflows/sla_and_views.html#organization-exists-in-sfdc-but-support-level-does-not-match-zendesk) in such cases as well, or, create an issue in the [support-ops project](https://gitlab.com/gitlab-com/support/support-ops/support-ops-project/-/issues/new).
1. If the ticket is from a customer with a valid support level (not in `Community`, `Expired` or `Hold`), but still doesn't have an SLA:
   1. [ ] Read this section to learn more: [No SLA](https://about.gitlab.com/handbook/support/workflows/sla_and_views.html#no-sla)

**Ticket Views**

Another important aspect of triaging tickets is making sure that they are in the right view. Tickets have to be in appropriate views to be able to receive attention from the right set of folks.

1. [ ] Take a look at the current Zendesk views we use in GitLab Support: [Current Zendesk Views](https://about.gitlab.com/handbook/support/support-ops/documentation/zendesk_global_views.html#current-views)
1. [ ] Organizations that have multiple subscriptions associated with them will result in tickets that receive _all_ the tags associated with that org. For instance, the ticket will have both `silver` and `premium` tags associated with it, resulting in multiple views.
   1. [ ] Read this section to understand how to handle such tickets: [Organizations with multiple subscriptions](https://about.gitlab.com/handbook/support/workflows/sla_and_views.html#organizations-with-multiple-subscriptions)
1. [ ] Similarly, a `Priority Prospect` ticket might show up in both SM and SaaS views. Read this section to understand how to handle such tickets: [Priority prospects showing in multiple views](https://about.gitlab.com/handbook/support/workflows/sla_and_views.html#priority-prospects-showing-in-multiple-views)
1. [ ] Read this section to know the checklist to follow to troubleshoot tickets ending up in the wrong view: [Wrong view](https://about.gitlab.com/handbook/support/workflows/sla_and_views.html#wrong-queue)
## Stage 3: It is time to get my hands on them tickets!

Set aside time to focus on triaging tickets every day for the next two weeks.

1. [ ] Add the link to at least 5 tickets that you triaged:
   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __

## Final stage - Tada!

**Summary**

Please review each point below, and check it off.

1. [ ] We receive tickets not just from paying customers, but also from other users. To make sure we are appropriately handling each, triaging a ticket before beginning technical work on it is very important.
1. [ ] Start with making sure the correct Ticket Form is being used.
1. [ ] Identify if the ticket is from a customer, prospect, trial user or free user.
1. [ ] Make sure that the triaged ticket is receiving the right SLA.
1. [ ] Make sure that the triaged ticket is in the right Zendesk View.
1. [ ] When changing anything related to ticket metadata, make an internal comment on the ticket as it will help the next engineer to understand the context.
1. [ ] You can switch between `Conversations` and `Events` on tickets in Zendesk to see what changed when and by whom.
1. [ ] Feel free to reach out to our friendly Support Ops team through [#support_operations](https://gitlab.slack.com/archives/C018ZGZAMPD) slack channel if you are unsure about something!

**Feedback**

1. [ ] Any updates or improvements needed? If there are any dead links, out of date or inaccurate content, missing content whether in this module or in other documentation, list it below as tasks for yourself! Once ready, have a [maintainer or manager review](https://gitlab.com/gitlab-com/support/support-training#guideline-to-update-support-training-module).
   1. [ ] Update ...
1. [ ] Manager: schedule a call (or integrate into 1:1) to review how the module went.

/label ~module
/label ~"Module::Triaging Tickets"
