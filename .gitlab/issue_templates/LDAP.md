---
module-name: "LDAP"
area: "Product Knowledge"
gitlab-group: "Manage:Access"
maintainers:
  - harishsr
---

## Overview

**Goal**: Set a clear path for LDAP Expert training

**Objectives**: At the end of this module, you should be able to:
- <insert a few concrete actions that learner should be able to do at the end of this module>

Remember to contribute to any documentation that needs updating

## Stage 0: Create and commit to the module

1. [ ] Create an issue using this template by making the Issue Title: LDAP - <your name>
1. [ ] Add yourself and your trainer as the assignees.
1. [ ] Notify your manager to let them know you've started.
1. [ ] Commit to this by notifying the current experts that they can start routing non-technical LDAP questions to you.
1. [ ] Optional: Set a milestone, if applicable, and a due date to help motivate yourself!

## Stage 1: Become familiar with what LDAP is

- [ ] **Done with Stage 1**

1. [ ] Understanding LDAP
   - [ ] Read [What is LDAP-Overview](https://en.wikipedia.org/wiki/Lightweight_Directory_Access_Protocol)
   - [ ] Read [Understanding the LDAP Protocol, Data Hierarchy, and Entry Components](https://www.digitalocean.com/community/tutorials/understanding-the-ldap-protocol-data-hierarchy-and-entry-components)
1. [ ] Using LDAP with GitLab
   - [ ] Watch the [Training Video](https://drive.google.com/open?id=0B9T-nz-7uMATSUxGT3hDX3RKdTg) Note: If needed, [get a license](https://about.gitlab.com/handbook/developer-onboarding/#working-on-gitlab-ee)
   - [ ] Watch [How to Manage LDAP, Active Directory in GitLab](https://www.youtube.com/watch?v=HPMjM-14qa8)
   - [ ] Read through all the [LDAP Documentation](https://docs.gitlab.com/ee/administration/auth/ldap/index.html) noting differences between CORE and EE specific LDAP features.
   - [ ] Read through how to configure [Google Secure LDAP](https://docs.gitlab.com/ee/administration/auth/ldap/google_secure_ldap.html).
   - [ ] Watch the Support Authentication Deep Dive (recorded June 2020) and review the accompanied slides:
      - [ ] [Session 1 of Deep Dive](https://drive.google.com/file/d/16hDb4lHXril_1UmchI5_NlH8iN_b6Kdl/view?usp=sharing)
      - [ ] [Session 2 of Deep Dive](https://drive.google.com/file/d/1nNFX-v1AvCaoibDrcw57jxD59BR-sT8Z/view?usp=sharing)
      - [ ] [Deep Dive Slides](https://docs.google.com/presentation/d/1S8IrmKBLMOsSxEJNQHBLja1ax3qZ0YFicUBM_RFtvVg/edit?usp=sharing)

## Stage 2: Technical Setup

- [ ] **Done with Stage 2**

1. Set up the GDK with an OpenLDAP server in your dev environment.

   1. [ ] Install the [GDK](https://gitlab.com/gitlab-org/gitlab-development-kit)
   1. [ ] Add [OpenLDAP](https://gitlab.com/gitlab-org/gitlab-development-kit/-/blob/main/doc/howto/ldap.md) to your GDK installation.
   1. [ ] Try [Group sync](https://docs.gitlab.com/ee/administration/raketasks/ldap.html#run-a-group-sync) in the GDK environment. If you don't succeed, contact an
   LDAP expert, then contribute to the documentation on this.
   1. [ ] Run a [UserSync](https://docs.gitlab.com/ee/administration/auth/ldap/ldap-troubleshooting.html#sync-all-users) and a [GroupSync in the rails console](https://docs.gitlab.com/ee/administration/auth/ldap/ldap-troubleshooting.html#sync-all-groups).

## Stage 3: Tickets

- [ ] **Done with Stage 3**

1. [ ] Find 10 Solved tickets to get a sense of what gets asked and how others have done troubleshooting in this area.
   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __
1. [ ] Answer 5 tickets on this module's topic and paste the links here. Do this even if a ticket seems too advanced for you to answer. Find the answers from an expert and relay them to the customers.
   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __

## Stage 4: Pair on Customer Calls (Optional)

- [ ] **Done with Stage 4**

1. [ ] Pair on two Diagnostic calls, where a customer has a problem with LDAP.
   1. [ ] call with ___ on ticket ___
   1. [ ] call with ___ on ticket ___

## Stage 5: Quiz

- [ ] **Done with Stage 5**

1. [ ] Take the LDAP quiz: https://forms.gle/PAEVfZYTwKA6TRmJ8
1. [ ] Review your answers. Reach out to your training or an expert if you have any questions.

## Penultimate stage: Review

Any updates or improvements needed? If there are any dead links, out of date or inaccurate content, missing content whether in this module or in other documentation, list it below as tasks for yourself! Once ready, have a [maintainer or manager review](https://gitlab.com/gitlab-com/support/support-training#guideline-to-update-support-training-module).

1. [ ] Update ...

## Final stage: Completion

1. [ ] Have your trainer review your tickets. If you do not have a trainer, ask an expert to review. Ask them to comment in this issue that you're either ready to be an expert, or state what else is needed. Check this box once they agree that you're ready.
1. [ ] Manager: schedule a call (or integrate into 1:1) to review how the module went.
1. [ ] Submit a MR to update `modules` and `knowledge_areas` in the [Support Team yaml file](https://gitlab.com/gitlab-com/support/team/-/blob/master/data/support-team.yaml) with this training module's topic. You will now be listed as an expert in LDAP on [Skills by Person page](https://gitlab-com.gitlab.io/support/team/skills-by-person.html).

## Logical Next Steps

1. OmniAuth and SAML: OmniAuth is often used with LDAP and its implementation is similar enough that it's a good idea to learn this next.
  - Setting up a [SAML](https://docs.gitlab.com/ee/integration/saml.html).
  - Setting up [OmniAuth](https://docs.gitlab.com/ee/integration/omniauth.html).
  - [Linking LDAP users](https://docs.gitlab.com/ee/integration/omniauth.html#initial-omniauth-configuration) to an OmniAuth account.
  - Understand the common configuration of using SAML for auth and LDAP groups for GroupSync.

/label ~module
/label ~"Module::LDAP"
/label ~"group::authentication and authorization" 
