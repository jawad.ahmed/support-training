---
module-name: "GitLab Pages"
area: "Product Knowledge"
gitlab-group: "Release:Release"
maintainers:
  - faleksic
---

## Overview

**Goal**: Set a clear path for GitLab Pages Expert training

**Objectives:**

- Learn about GitLab Pages and how to utilize it.
- Learn some of the more complex tasks of using GitLab Pages.
- Learn how to use artifacts with GitLab Pages.

---

## Stage 0: Create and commit to the module

1. [ ] Create an issue using this template by making the Issue Title: GitLab Pages - <your name>
1. [ ] Add yourself and your trainer as the assignees.
1. [ ] Notify your manager to let them know you've started.
1. [ ] Commit to this by notifying the current experts that they can start routing non-technical GitLab Pages questions to you.
1. [ ] Optional: Set a milestone, if applicable, and a due date to help motivate yourself!

## Stage 1: Become familiar with what GitLab Pages are

- [ ] **Done with Stage 1**

1. [ ] Learn about GitLab Pages
    1. [ ] Read [GitLab Pages](https://docs.gitlab.com/ee/user/project/pages/)
    1. [ ] Read [Custom domains and SSL/TLS Certificates](https://docs.gitlab.com/ee/user/project/pages/custom_domains_ssl_tls_certification/index.html)
    1. [ ] Read [GitLab Pages integration with Let's Encrypt](https://docs.gitlab.com/ee/user/project/pages/custom_domains_ssl_tls_certification/lets_encrypt_integration.html)
    1. [ ] Read [GitLab Pages Access Control](https://docs.gitlab.com/ee/user/project/pages/pages_access_control.html)
    1. [ ] Read [GitLab Pages administration](https://docs.gitlab.com/ee/administration/pages/index.html)
    1. [ ] Review the [Category Direction](https://about.gitlab.com/direction/release/pages/)
          for GitLab Pages
    1. [ ] Review [GitLab Pages Examples](https://gitlab.com/pages)
1. [ ] Watch videos
    1. [ ] Watch [How to Enable GitLab Pages for GitLab CE and EE](https://www.youtube.com/watch?v=dD8c7WNcc6s)

## Stage 2: Technical setup

* [ ] **Done with Stage 2**

1. [ ] Setup a self-managed instance to use GitLab Pages. Ensure you play with:
    1. Access control
    1. Using both self-signed and Let's Encrypt SSLs
    1. Using a custom domain
1. [ ] Make projects that utilize GitLab Pages
    1. [ ] One using the Plain HTML generator
    1. [ ] One using a different generator template of your choice
    1. [ ] One made from scratch that uses an artifact generated in a previous
        job.

## Stage 3: Tickets

1. [ ] Find 10 Solved tickets to get a sense of what gets asked and how others have done troubleshooting in this area.
   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __
1. [ ] Answer 5 tickets on this module's topic and paste the links here. Do this even if a ticket seems too advanced for you to answer. Find the answers from an expert and relay them to the customers.
   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __

## Stage 4: Quiz

1. [ ] Find someone experienced with Pages to be your trainer. You can use the [Skills by Subject](https://gitlab-com.gitlab.io/support/team/skills-by-subject.html) page to identify candidates.
1. [ ] On [GitLab.com](https://gitlab.com), create 2 projects that will use
        GitLab Pages:
    1. [ ] Project 1
      1. [ ] Utilizes access control so that only yourself, your manager, and
              the trainer who reviews this can see it.
      1. [ ] Utilizes the Plain HTML generator to make a simple website that
              shows an image (the image itself is your choice).
    1. [ ] Project 2
      1. [ ] Utilizes access control so that only yourself, your manager, and
              the trainer who reviews this can see it.
      1. [ ] Utilizes a generator of your choice
      1. [ ] Utilizes an artifact that dynamically pulls the latest commit ID
              for said project.
1. [ ] On a self-managed instance, create a project that uses GitLab Pages
    1. [ ] The instance must be using a self-signed SSL
    1. [ ] The instance must force a redirect from http to https
1. [ ] Once you have completed this, have the trainer review what you have
        created above and then comment below acknowledging your success.

## Penultimate stage: Review

Any updates or improvements needed? If there are any dead links, out of date or inaccurate content, missing content whether in this module or in other documentation, list it below as tasks for yourself! Once ready, have a [maintainer or manager review](https://gitlab.com/gitlab-com/support/support-training#guideline-to-update-support-training-module).

1. [ ] [Create one or more MRs](https://docs.gitlab.com/ee/development/documentation/) with improvements to Pages-related documentation.
1. [ ] Update ...

## Final Stage:

1. [ ] Have your trainer review your tickets and assessment. If you do not have a trainer, ask an expert to review.
1. [ ] Manager: schedule a call (or integrate into 1:1) to review how the module went.
1. [ ] Submit a MR to update `modules` and `knowledge_areas` in the [Support Team yaml file](https://gitlab.com/gitlab-com/support/team/-/blob/master/data/support-team.yaml) with this training module's topic. You will now be listed as an expert in GitLab Pages on [Skills by Person page](https://gitlab-com.gitlab.io/support/team/skills-by-person.html).

/label ~module
/label ~"Module::GitLab Pages"
