---
module-name: "SAML"
area: "Product Knowledge"
gitlab-group: "Manage:Access"
maintainers:
  - cynthia
  - anton
---

## Overview

**Goal**: Set a clear path for SAML SSO Expert training

**Objectives**: At the end of this module, you should be able to:
- Understand how GitLab leverages the OmniAuth gem with a SAML Strategy to act as a SAML 2.0 Service Provider
- Understand how to set up SAML apps for SSO for Groups
- Troubleshoot customer's issues with SAML

This is a prerequisite to the SCIM module. Remember to contribute to any documentation that needs updating.

## Stage 0: Create and commit to the module

1. [ ] Create an issue using this template by making the Issue Title: SAML SSO - <your name>
1. [ ] Add yourself and your trainer as the assignees.
1. [ ] Notify your manager to let them know you've started.
1. [ ] Optional: Commit to this by notifying the current experts that they can start routing SAML SSO questions to you.
1. [ ] Optional: Set a milestone, if applicable, and a due date to help motivate yourself!

## Stage 1: Become familiar with what SAML is

- [ ] **Done with Stage 1**

1. [ ] Read through the [GitLab SAML Documentation](https://docs.gitlab.com/ee/integration/saml.html).
1. [ ] Read through the [omniauth-saml gem documentation](https://github.com/omniauth/omniauth-saml).
1. [ ] Read through the [GitLab SAML SSO for Groups Documentation](https://docs.gitlab.com/ee/user/group/saml_sso/).
1. [ ] Watch the [Manage 201 SAML knowledge sharing](https://www.youtube.com/watch?v=CW0SujsABrs). You can access the [slides](https://docs.google.com/presentation/d/10uUGoDB4nN0Ho42vwq3-aTO5UMO5a3Bjgor8maWe-zw/edit#slide=id.g444cb56ba0_0_0) as well.
1. [ ] Watch the Support Authentication Deep Dive (recorded Dec 2022) and review the accompanied slides:
      - [ ] [Session 1 of Deep Dive](https://www.youtube.com/watch?v=CHE6nPozJdo)
      - [ ] [Session 2 of Deep Dive](https://www.youtube.com/watch?v=gk1vwmb8qNk)
      - [ ] [Deep Dive Slides](https://docs.google.com/presentation/d/1FZGVkf56MGc2AJPJ0tMQQEoyteVpAD57ZQYRnqRiLYg/edit?skip_itp2_check=true&pli=1#slide=id.p1)


### Slides
- [SAML Deep Dive slides](https://docs.google.com/presentation/d/1FZGVkf56MGc2AJPJ0tMQQEoyteVpAD57ZQYRnqRiLYg/edit?skip_itp2_check=true&pli=1#slide=id.p1)

## Stage 2: Technical Setup

- [ ] **Done with Stage 2**

1. [ ] Implement SAML

  SAML works on both GitLab.com and Self-managed. But it's much easier
to do all tests on self- managed once you setup your instance. No restrictions or having to use ultimate/premium trials on groups like in SaaS.

  - Note: If you do encounter a scenario where you want or need to test SAML/SSO on a top-level paid GitLab.com group, follow the instructions in [Testing Environment: on GitLab.com section](https://about.gitlab.com/handbook/support/workflows/test_env.html#testing-on-gitlabcom).

  - Note: If using GDK, follow the [SAML How To Documentation](https://gitlab.com/gitlab-org/gitlab-development-kit/-/blob/main/doc/howto/saml.md). If you prefer,
        you can use the same Docker images but with a non-GDK instance of GitLab.

  - Note: If you are using support resources, you would need to [create a firewall rule](https://cloud.google.com/vpc/docs/using-firewalls#creating_firewall_rules)
        to allow the custom ports.

  - Note: Refer to the [Testing Environment](https://about.gitlab.com/handbook/support/workflows/test_env.html) page within the Support Team Handbook for details on [Azure](https://about.gitlab.com/handbook/support/workflows/test_env.html#azure-testing-environment) or [Okta](https://about.gitlab.com/handbook/support/workflows/test_env.html#okta-testing-environment) access for SAML testing purposes.

    1. [ ] Set up instance-wide SAML on your GitLab instance.
    1. [ ] Set up [Group SAML](https://docs.gitlab.com/ee/integration/saml.html#configuring-group-saml-on-a-self-managed-gitlab-instance) on your GitLab instance.
              Note: You can check our docs to configure group SAML for [Omnibus Installation](https://docs.gitlab.com/ee/integration/saml.html#omnibus-installations)
              and [Source Installations](https://docs.gitlab.com/ee/integration/saml.html#source-installations).
    1. [ ] Contribute to the documentation with any issues or troubleshooting steps.

      The most used IdPs that you would deal with in tickets are `Azure` and `Okta`. Pick one of them and comment out the other. (Your choice is: _______ )

    1. [ ] Setup and configure [Group SAML](https://docs.gitlab.com/ee/user/group/saml_sso/).
    1. [ ] Setup Azure or Okta. Refer to [Azure Setup notes](https://docs.gitlab.com/ee/user/group/saml_sso/#azure-setup-notes) or [Okta Setup notes](https://docs.gitlab.com/ee/user/group/saml_sso/#okta-setup-notes), and [troubleshooting group SAML](https://docs.gitlab.com/ee/administration/troubleshooting/group_saml_scim.html).
    1. [ ] Create subgroups in your group and Setup [Group Links](https://docs.gitlab.com/ee/user/group/saml_sso/#group-sync).

## Stage 3: Tickets

- [ ] **Done with Stage 3**

1. [ ] Go through 10 solved SAML/SSO tickets to check the responses and get a sense
of the types of frequently asked questions that come up.
   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __
1. [ ] Answer 5 SAML/SSO tickets and paste the links here, even if a ticket seems
too advanced for you to answer. Find the answers from an expert and relay them to
the customers. Tickets should cover at least 2 different SAML providers, and both SaaS and Self-Managed.
   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __

## Stage 4: Pair on Customer Calls (Optional)

- [ ] **Done with Stage 4**

1. [ ] Pair on two calls, where a customer has a problem with SAML/SSO.
   1. [ ] call with ___
   1. [ ] call with ___

## Stage 5: Assessment

Note: Please do not look at the assessment until you are ready to complete it.

- [ ] **Done with Stage X**

1. [ ] Complete [the assessment](https://docs.google.com/forms/d/e/1FAIpQLScGTRLkmPXXS_BsgFBsJ2Fk3Ja0wemFO6G0pQIAf7kOwfw0Rw/viewform).
  - If you are linked to a Google form, please complete the self-assessment. If you have any questions about the answers, please ask your trainer or an expert.
  - If something is incorrect, please consult with your trainer or an expert and fix it in the [Support Team Drive Training/Training Module Assessments folder](https://drive.google.com/drive/u/0/folders/147D5ecSDsV2J9OCN_6t-J4vYzPLTebue).

## Penultimate Stage: Review

Any updates or improvements needed? If there are any dead links, out of date or inaccurate content, missing content whether in this module or in other documentation, list it below as tasks for yourself! Once ready, have a [maintainer or manager review](https://gitlab.com/gitlab-com/support/support-training#guideline-to-update-support-training-module).

- [ ] Update ...

## Final Stage

1. [ ] Have your trainer review your assessment. If you do not have a trainer, ask an expert to review.
1. [ ] Manager: schedule a call (or integrate into 1:1) to review how the module went.
1. [ ] Submit a MR to update `modules` and `knowledge_areas` in the [Support Team yaml file](https://gitlab.com/gitlab-com/support/team/-/blob/master/data/support-team.yaml) with this training module's topic. You will now be listed as an expert in SAML on [Skills by Person page](https://gitlab-com.gitlab.io/support/team/skills-by-person.html).

SAML is often used together with SCIM so it may be worth starting [the SCIM module](https://gitlab.com/gitlab-com/support/support-training/-/blob/master/.gitlab/issue_templates/SCIM.md) next.

/label ~module
/label ~"Module::SAML"
/label ~"group::authentication and authorization" 
