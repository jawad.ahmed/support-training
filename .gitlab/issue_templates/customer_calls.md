---
module-name: "customer-calls"
area: "Customer Service"
gitlab-group: "n/a"
maintainers:
  - lyle
---

## Overview

**Goal**: Support Engineers who complete this module should have the ability to organize successful calls with customers.
   
*Length*: 
   - Content: 1-2 hours
   - Calls: 5-10 hours
   - Total: 6-12 hours

**Objectives**: At the end of this module, you should be able to:
- understand emotional headwinds that delay or prevent calls from happening
- identify situations where a call is needed or advisable
- guide customers towards negating the need for calls at all
- set and manage customer expectations prior to the call in regards to call length and purpose
- make calls successful
- end calls gracefully
- get manager help when calls can't be ended gracefully

## Stage 0: Create and commit to the module

1. [ ] Create an issue using this template by making the Issue Title: `Customer Calls - <your name>`
1. [ ] Add yourself and your trainer as the assignees.
1. [ ] Notify your manager to let them know you've started.
1. [ ] Commit to this by notifying your SGG that you've started this module and are looking to shadow or lead customer calls.
1. [ ] Optional: Set a milestone, if applicable, and a due date to help motivate yourself!

Consider using the Time Tracking functionality so that the estimated length for the module can be refined.

## Stage 1: Emotional Headwinds

In this section we acknowledge some of the emotional barriers to taking customer calls.

1. [ ] Review the [emotional headwinds section of the customer call workflow](https://about.gitlab.com/handbook/support/workflows/customer_calls.html#emotional-headwinds)
1. [ ] If you have not completed the **Customer Service Skills** training module:
   - Watch videos 1.11 - 1.14 on Core Human needs from the [Support Training - Ticket Management and Communications Playlist](https://www.youtube.com/playlist?list=PL05JrBw4t0Kq13oaMq0DCl2gUz_g1u29o)
1. [ ] **Exercise** Reflect on the content of this section, and in a comment on this issue or in a video upload to GitLab Unfiltered: 
   - Articulate the things that make you the most nervous about taking customer calls. 
   - If you have encountered a situation where your fears have been realized, describe the situation.
   - Think through (and describe) some strategies for how you might overcome or walk through the feelings generated by customer calls.

## Stage 2: When should tickets go to a call?

In this section we examine markers for when you should strongly consider moving to a call.

1. [ ] Review the [When should tickets go to a call? section of the customer call workflow](https://about.gitlab.com/handbook/support/workflows/customer_calls.html#when-should-a-ticket-go-to-a-call)
1. [ ] If you have not completed the **Customer Service Skills** training module:
   - Watch videos 2.03 - 2.03b on Synchronous Communication from the [Support Training - Ticket Management and Communications Playlist](https://www.youtube.com/playlist?list=PL05JrBw4t0Kq13oaMq0DCl2gUz_g1u29o)
   - Watch videos 3.02 - 3.02a on Understanding the Customer's Priorities from the [Support Training - Ticket Management and Communications Playlist](https://www.youtube.com/playlist?list=PL05JrBw4t0Kq13oaMq0DCl2gUz_g1u29o)
1. [ ] **Exercise** Reflect on the content of this section, and in a comment on this issue or in a video upload to GitLab Unfiltered: 
   - Tell about a time when (as a customer) you needed customer support and really needed to talk to a human. What barriers were in place? Were they reasonable? When you did get to a human, what value did they add to your support interaction?
   - Point to a ticket where you have seen one of [the markers for when you should consider moving to a call](https://about.gitlab.com/handbook/support/workflows/customer_calls.html#markers-for-when-you-should-consider-moving-to-a-call) and a call did not happen. What was the result?

## Stage 3: Types of calls and the technical details of organizing them

In this section we take a look at the ins-and-outs of the types of calls, scheduling them, and the small technical things that keep everything running smoothly -- all the "easy" stuff that does not actually involve talking to anyone.

1. [ ] Review the [Types of Calls section of the customer call workflow](https://about.gitlab.com/handbook/support/workflows/customer_calls.html#types-of-calls)
1. [ ] Review [Scheduling the call flowchart](https://about.gitlab.com/handbook/support/workflows/customer_calls.html#scheduling-the-call)
1. [ ] Review the [Providing the call link](https://about.gitlab.com/handbook/support/workflows/customer_calls.html#providing-the-call-link) section and get set up in Calendly if you have not already done so.
1. [ ] Review the [Pre-call email](https://about.gitlab.com/handbook/support/workflows/customer_calls.html#pre-call-email) section.
1. [ ] Review the [Post-call](https://about.gitlab.com/handbook/support/workflows/customer_calls.html#post-call) section.
1. [ ] If you have not completed the **Customer Service Skills** training module:
   - Watch videos 2.14 on meeting followup from the [Support Training - Ticket Management and Communications Playlist](https://www.youtube.com/playlist?list=PL05JrBw4t0Kq13oaMq0DCl2gUz_g1u29o)
   - Watch video 6.02 on meeting followup from the [Support Training - Ticket Management and Communications Playlist](https://www.youtube.com/playlist?list=PL05JrBw4t0Kq13oaMq0DCl2gUz_g1u29o)
1. [ ] Review the tips for [customer no shows](https://about.gitlab.com/handbook/support/workflows/customer_calls.html#customer-no-shows)
1. [ ] If you have not completed the **Customer Service Skills** training module:
   - Watch video 4.05b on customer no show from the [Support Training - Ticket Management and Communications Playlist](https://www.youtube.com/playlist?list=PL05JrBw4t0Kq13oaMq0DCl2gUz_g1u29o)
1. [ ] Review the [Audio and Video Guidelines for Support Engineers](https://about.gitlab.com/handbook/support/workflows/customer_calls.html#audio-and-video-guidelines-for-support-engineers-on-customer-calls) section.
1. [ ] Make sure you are aware of [how to get manager help](https://about.gitlab.com/handbook/support/workflows/customer_calls.html#getting-manager-help) if you need it.
1. [ ] **Exercise** Affirm the following statements:
   - [ ] The event title(s) I use in Calendly include `Support` and have a required field for the ZD ticket number.
   - [ ] I will use the appropriate macros to make sure tickets are tagged for reporting.
   - [ ] I will generate single-use Calendly links so that I do not get unexpected calls on my calendar.
   - [ ] My AV setup is set up well (or I expensed some items to improve the quality).
   - [ ] I know how to get manager help!

## Stage 4: Taking calls

In this section we examine some ideas that will help calls go smoothly and end well ( in the amount of time you anticipated).

1. [ ] Review the [Removing the need for a call before it even starts](https://about.gitlab.com/handbook/support/workflows/customer_calls.html#removing-the-need-for-a-call-before-it-even-starts) section.
1. [ ] Read about setting expections within the call to [keep calls within the scheduled time](https://about.gitlab.com/handbook/support/workflows/customer_calls.html#tips-to-keep-calls-within-the-scheduled-time).
1. [ ] Read through the [tips for making calls successful](https://about.gitlab.com/handbook/support/workflows/customer_calls.html#tips-for-making-calls-successful) section.
1. [ ] If you have not completed the **Customer Service Skills** training module:
   - [ ] Watch video 2.06 on Setting Expectations from the [Support Training - Ticket Management and Communications Playlist](https://www.youtube.com/playlist?list=PL05JrBw4t0Kq13oaMq0DCl2gUz_g1u29o)
   - [ ] Watch videos 4.02 - 4.03a on Time management in live calls from the [Support Training - Ticket Management and Communications Playlist](https://www.youtube.com/playlist?list=PL05JrBw4t0Kq13oaMq0DCl2gUz_g1u29o) 
    - [ ] Watch video 4.04 (from 6:15) on Bringing the customer with you in customer calls from the [Support Training - Ticket Management and Communications Playlist](https://www.youtube.com/playlist?list=PL05JrBw4t0Kq13oaMq0DCl2gUz_g1u29o)
 1. [ ] **Exercise** Reflect on the content of this section, and in a comment on this issue or in a video upload to GitLab Unfiltered: 
    - Tell us about a time when you failed to set expectations with the customer and their own expectations took over. 


## Stage 5: Putting it into practice

In this section, we'll take what we've covered and put it into practice!

1. [ ] Pair or shadow on 5 customer calls. To the extent that you can, take a variety of [types of calls](https://about.gitlab.com/handbook/support/workflows/customer_calls.html#types-of-calls).
   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __
1. [ ] Lead 5 customer calls
   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __
   1. [ ] __

## Penultimate stage: Review

Any updates or improvements needed? If there are any dead links, out of date or inaccurate content, missing content whether in this module or in other documentation, list it below as tasks for yourself. Once ready, have a [maintainer or manager review](https://gitlab.com/gitlab-com/support/support-training#guideline-to-update-support-training-module).

1. [ ] Update ...

## Final stage: Completion

1. [ ] Have your trainer review your tickets and assessment. If you do not have a trainer, ask an expert to review.
1. [ ] Manager: schedule a call (or integrate into 1:1) to review how the module went.
1. [ ] Submit a MR to update `modules` and `knowledge_areas` in the [Support Team yaml file](https://gitlab.com/gitlab-com/support/team/-/blob/master/data/support-team.yaml) with this training module's topic. You will now be listed as an expert in this topic on [Skills by Person page](https://gitlab-com.gitlab.io/support/team/skills-by-person.html).

/label ~module ~"Module::Customer Calls"
