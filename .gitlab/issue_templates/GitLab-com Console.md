---
module-name: "GitLab-com Console"
area: "Product Knowledge"
maintainers:
  - arihantar
  - cynthia
---


### Overview

----

**NOTE**: Full read/write access to the production console is no longer being provided to Support Engineers as of 2022. [Teleport](https://gitlab.com/gitlab-com/runbooks/-/blob/master/docs/teleport/Connect_to_Rails_Console_via_Teleport.md) currently only provides **read-only access**. Any requests that require write access must go through Support Engineers who have previously been given the ability or infra. Please see [this epic](https://gitlab.com/groups/gitlab-com/gl-infra/-/epics/350) for infra's progress on read/write teleport access and [this issue](https://gitlab.com/gitlab-com/support/support-team-meta/-/issues/3860) for details on some differences. After completing this module, you may not be able to complete all tasks but we encourage you to continue pairing!

----

Your manager's approval _must_ be obtained before starting this module. Basic Ruby, Ruby on Rails and [Ruby Interactive Shell](https://www.digitalocean.com/community/tutorials/how-to-use-irb-to-explore-ruby) knowledge are minimum requirements as all work is done in production Rails console environments.

**Goal**: This mini-module is meant to provide guidance on getting started with GitLab.com console.

**Objectives**: At the end of this module, you should be able to:

- follow console related requests.
- resolve Support Engineer Escalations for GitLab.com through common console related requests.

Start with Stage 1. Stages 2 and 3 can be done simulatenously, **but first**:

### Stage 0: Prerequisites: 

1. [ ] After obtaining approval, assign yourself and your manager to this issue.
1. [ ] Optional: Set milestones, if applicable, and a due date to help motivate yourself!
1. [ ] Familiarize youself with ruby and interactive Ruby: (Optional if you are experienced with Ruby, and you don't have to do all)
    1. [ ] [Ruby quick start](https://www.ruby-lang.org/en/documentation/quickstart/)
    1. [ ] [Ruby quick guide](https://www.tutorialspoint.com/ruby/ruby_quick_guide.htm)
    1. [ ] [Ruby Interactive Shell](https://www.digitalocean.com/community/tutorials/how-to-use-irb-to-explore-ruby)

### Stage 1: Get primed and ready

- [ ] **Done with Stage 1**

1. [ ] Have an existing Owner add you to the `gitlab-com/support/dotcom/console` group as an `Owner` so that you are a direct member.
1. [ ] [Subscribe](https://about.gitlab.com/blog/2016/04/13/feature-highlight-subscribe-to-label/) to the following labels (if not subscribed to all issues in internal requests project):
    1. [Console Escalation::GitLab.com label](https://gitlab.com/gitlab-com/support/internal-requests/-/labels?subscribed=&search=console+)
    1. [DEWR label](https://gitlab.com/gitlab-com/support/internal-requests/-/labels?subscribed=&search=DEWR) (Dotcom Escalation Weekly Report)
1. [ ] Open an [access request](https://about.gitlab.com/handbook/business-technology/team-member-enablement/onboarding-access-requests/access-requests/) for `Teleport Console - Production` and assign it to your manager per the instructions.

### Stage 2: Related reading and resources

- [ ] **Done with Stage 2**

1. Review these resources. Bookmark them to keep them handy:
    1. [ ] [Rails Console Guide](https://docs.gitlab.com/ee/administration/troubleshooting/navigating_gitlab_via_rails_console.html)
    1. [ ] [Rails Console Cheat Sheet](https://docs.gitlab.com/ee/administration/troubleshooting/gitlab_rails_cheat_sheet.html)
    1. [ ] Runbooks: 
         1. [ ] [Support runbooks](https://gitlab.com/gitlab-com/support/runbooks/-/tree/master/code)
         1. [ ] [More runbooks](https://gitlab.com/gitlab-com/runbooks/-/tree/master/docs/uncategorized)   
1. [ ] Review [when to escalate to infra](https://gitlab.com/gitlab-com/gl-infra/infrastructure/-/issues/9477) (this currently links to an issue where it's being discussed, and should be updated to a workflow later). Typically, if you're working on an issue and get stuck, reach out to another member first.
1. [ ] Review [Accessing the rails console via Teleport](https://gitlab.com/gitlab-com/runbooks/-/blob/master/docs/teleport/Connect_to_Rails_Console_via_Teleport.md), whenever GitLab.com console access is required, this is the process you will follow. Remember to do any testing on your own instance.
1. [ ] GitLab is a big product and we don't have simple database diagram that can be used. However you can take a look at the [structure.sql file](https://gitlab.com/gitlab-org/gitlab/-/blob/master/db/structure.sql) (consider browsing it locally rather in a browser) while paying attention to common pieces like users (search for `CREATE TABLE users`) and projects (search for `CREATE TABLE projects`).
1. Review these Console Escalation issues to get a better understanding of what is often asked and how to troubleshoot.
    1. [ ] [Force delete project](https://gitlab.com/gitlab-com/support/internal-requests/-/issues/1882)
    1. [ ] [Manual project deletion](https://gitlab.com/gitlab-com/support/internal-requests/-/issues/11373)
    1. [ ] [Delete uploaded file](https://gitlab.com/gitlab-com/support/internal-requests/-/issues/1728)
    1. [ ] [Project Export](https://gitlab.com/gitlab-com/support/internal-requests/-/issues/6913)
    1. [ ] [More complex batch version of forcing confirmation](https://gitlab.com/gitlab-com/support/internal-requests/-/issues/1826)
    1. [ ] [Force update project statistics](https://gitlab.com/gitlab-com/support/internal-requests/-/issues/16089)
    1. [ ] [Clear failed project deletion status](https://gitlab.com/gitlab-com/support/internal-requests/-/issues/17757)
    1. [ ] [Find membership details](https://gitlab.com/gitlab-com/support/internal-requests/-/issues/17627)

### Stage 3: Working on issues

- [ ] **Done with Stage 3**

Check these off to acknowledge:

- [ ] Before working on a console escalation, ensure there is a relevant issue link that addresses why the action is being taken.  If not, ping the requester for one.
    - Console actions are meant to be a workaround or mitigation for a bug or (rarely) lack of feature, so we should never be taking action without an issue that clearly defines how the user is affected, and that all other possible workarounds have been exhausted.
- [ ] Before taking action, possible risks have been identified, and either a method to revert is available or risk has been deemed acceptable.
- [ ] All actions that will change or remove any data and its output will be copied to an issue. An internal request issue will be created if necessary.
- [ ] Functions that do not already exist [in the support runbooks](https://gitlab.com/gitlab-com/support/runbooks/-/tree/master/code) require a review. Reviewer can be outside support, such as dev or security.
    - Note: Using a loop to perform a routine action does not require review. Example: As part of a project deletion request, deleting all merge requests because the deletion worker is timing out due to the large number of MRs.
- [ ]  Make sure that you know how to involve another admins with R/W access.
    1. Mention `@gitlab-com/support/dotcom/console/write` in the issue itself to ping team members with write access to perform it (once you've prepared the commands). (Check the [list of all R/W alloweed personel](https://gitlab.com/groups/gitlab-com/support/dotcom/console/write/-/group_members?with_inherited_permissions=exclude))
    1. Ping SRE in #infrastructure-lounge in Slack if it's more urgent (or `@sre-oncall`)


1. [ ] Pair on internal issues that require console access. Create a pairing session and link it here.
   1. Pairing:

1. [ ] Answer 3 internal issues that require console access and paste the links here. Remember to paste the commands you run (and output if needed) as a comment in the issue.
   1.
   1.
   1.

### Penultimate Stage: Review

Any updates or improvements needed? If there are any dead links, out of date or inaccurate content, missing content whether in this module or in other documentation, list the needed updates below as tasks for yourself!

- [ ] Update ...

### Final Stage: Completion

1. [ ] Have your manager review this issue.
1. [ ] Manager: schedule a call (or integrate into 1:1) to review how the module went once you have reviewed this issue.
1. [ ] Submit a MR to update the [Support Team yaml file](https://gitlab.com/gitlab-com/support/team/-/blob/master/data/support-team.yaml) with this training module's topic under your list of completed `modules`.

/label ~module
/label ~"Module::GitLab-com Console"
