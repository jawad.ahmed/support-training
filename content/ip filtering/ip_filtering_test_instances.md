# IP Filtering a test instance on GCP and AWS

The following set of steps shows how you can edit the default Firewall rules in GCP and AWS to limit access to your instances via HTTP/HTTPS. As an alternative, you can leave the default rule untouched and create a separate rule using similar steps.  **If you're using LetsEncrypt for TLS, [configure it first](https://docs.gitlab.com/omnibus/settings/ssl.html) before configuring IP filtering for simplicity. LetsEncrypt will validate your web server from a [varying set of IP addresses](https://letsencrypt.org/docs/faq/#what-ip-addresses-does-let-s-encrypt-use-to-validate-my-web-server).  As an alternative, a self-signed certificate can be used.**

## GCP

Edit the default HTTPS ingress rule specified for GCP VPC Networks in your GCP Project, changing default `0.0.0.0/0` allowed to your external IP.

1. Open the [GitLab Sandbox](https://gitlabsandbox.cloud/cloud)
2. Use the Open Web Console button to redirect to your GCP console

    ![web-console-button.png](./web-console-button.png)

3. Navigate to the **Settings > VPC Network > Firewall** page:

    ![gcp-vpcnet-firewall.png](./gcp-vpcnet-firewall.png)

4. Locate and click on the existing default ingress rule for HTTPS traffic from the table of available firewall rules:

    ![gcp-default-https-firewall-rule.png](./gcp-default-https-firewall-rule.png)

5. Click Edit at the top of the resulting page:

    ![gcp-edit-firewall-rules.png](./gcp-edit-firewall-rules.png)

6. Under the section **Source IPv4 Rules**, replace the default `0.0.0.0/0` with your external IP. You can use a service like [WhatIsMyIPAddress.com](https://whatismyipaddress.com/) to find out what that is.

    ![gcp-input-allowed-ip4-ranges.png](./gcp-input-allowed-ip4-ranges.png)

7. **Save the rule.**

If you'd rather leave the default tag untouched and create your own rule to apply just to a specific VM, you can [create a firewall rule](https://cloud.google.com/vpc/docs/using-firewalls), give it a [specific tag](https://cloud.google.com/vpc/docs/firewalls#rule_assignment), and [apply that tag](https://cloud.google.com/vpc/docs/add-remove-network-tags#adding_viewing_and_removing_tags) to the test instance VM instead. 
Further reading on [GCP Firewall rules can be found here](https://cloud.google.com/vpc/docs/using-firewalls#creating_firewall_rules#console). 

## AWS

When you create an EC2 instance, a default security group is generated with generic rules to be applied to that instance. You can edit this security group, removing the default `0.0.0.0/0` and replacing it with your external IP in CIDR format (appended with /32) e.g. `101.177.27.80/32`.

1. Open https://gitlabsandbox.cloud/cloud
2. Use the Open Web Console button to redirect to your AWS console

    ![web-console-button.png](./web-console-button.png)

3. Navigate to the **Services > EC2** page:

    ![aws-services-ec2.png](./aws-services-ec2.png)

4. Navigate to the **Instances > Instances** page:

    ![aws-instances-instances-menu.png](./aws-instances-instances-menu.png)

5. Click on the *Instance ID* that you want to apply IP restriction to.
6. Under the tabbed section on the resulting page, click on the *Security* tab:

    ![aws-security-tab.png](./aws-security-tab.png)

7. Click on the *Security groups link* to be taken to the instances *Security Group*

    ![aws-instance-sec-groups.png](./aws-instance-sec-groups.png)

8. At the top right of the *Inbound Rules* table, click the *Edit inbound rules* button:

    ![aws-edit-inbound-rules.png](./aws-edit-inbound-rules.png)

9. Adjust the *HTTPS rule (TCP 443)* from `0.0.0.0/0` to your external IP with /32 appended to satisfy AWS' requirement of a CIDR block format. You can use a service like [WhatIsMyIPAddress.com](https://whatismyipaddress.com/) to find out what your external IP address is.

    ![aws-security-group-rule.png](./aws-security-group-rule.png)

10. **Save the rule.**

Further reading on [AWS Security Groups can be found here](https://docs.aws.amazon.com/vpc/latest/userguide/VPC_SecurityGroups.html).

### To consider

After IP filtering is enabled for HTTP/S traffic, communication to the instance is only allowed via the IP address ranges you've specified.

This change will affect any external service that might need to communicate to your testing instance. GitLab Runners, Elasticsearch servers, and other integrations are a few examples of services that may need consideration when IP filtering is enabled. You will need to consider how to include the external services IP addresses in the allowed ranges.

In the case of LetsEncrypt, the integration will periodically need to communicate to LetsEncrypts' public staging servers for certificate renewal (usually every 90 days). If you're finding that you're unable to successfully run the `gitlab-ctl reconfigure` command on your instance due to communication errors to LetsEncrypt ACME staging servers:

1. Add `0.0.0.0/0` back to your HTTP/S default rule
1. Run the `gitlab-ctl reconfigure`
1. Remove the `0.0.0.0/0` from the rule

This method will temporarily allow communication for LetsEncrypt to renew the SSL certificate.