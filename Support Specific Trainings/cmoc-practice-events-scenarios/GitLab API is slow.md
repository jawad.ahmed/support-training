# CMOC Practice event

This scenario is meant to be used during a [CMOC Practice Event](https://gitlab.com/gitlab-com/support/support-team-meta/-/blob/master/.gitlab/issue_templates/CMOC%20Practice%20Event.md).

More information about this process can be found at the [CMOC Practice Events](https://about.gitlab.com/handbook/support/workflows/cmoc_workflows.html#cmoc-training-activities) page.

Please verbally note to the practicing **CMOC** that we will **SKIP** the [Notify Stakeholders](https://about.gitlab.com/handbook/support/workflows/cmoc_workflows.html#notify-stakeholders) step in this practice event.

## Event Details

This event scenario covers an example S2 incident. The GitLab API is slow to respond and sometimes timing out.

## Scenario details

The person that acts as an **IM** should follow the steps outlined below. Let the practicing **CMOC** know they're free to ask any questions at any time as the purpose is to get familiar with the tooling and the workflow.

- **IM**: Start a `CMOC thread` in the incident channel.
- **IM**: Ask the practicing **CMOC** if they can share the screen so we can follow along and make sure we aren't working on the production status page.
- When both the **IM** and the practicing **CMOC** join the call double-check that any actions performed on the status.io page are done on the **Test Page**.
- **IM**: Inform the practicing **CMOC** that GitLab.com API is slow to respond but that the website seems to be responding for some pages.
- **IM**: Wait a moment and confirm with the practicing **CMOC** that this is production impacting and that customers will start to see issues.
  - The status page's state should be "Investigating".
  - The affected infrastructure should be "API".
- After the status page is created double-check if the practicing **CMOC** has added the label `~Incident-Comms::Status-Page` to the issue.
- **IM**: Inform the practicing **CMOC** that some time has passed and that we are still investigating the issue and that there isn't a workaround at this time.
- **IM**: Inform the practicing **CMOC** that at this point approximately 15 minutes has passed as they should be aware of the [Frequency of Updates](https://about.gitlab.com/handbook/support/workflows/cmoc_workflows.html#frequency-of-updates), ask them if there are any customer reports.
  - Practicing **CMOC** should go through the process of [searching Zendesk](https://about.gitlab.com/handbook/support/workflows/cmoc_workflows.html#monitor-zendesk) for tickets. (As long as they go through the process, we can say at the end they can pretend there is 5 customer tickets reporting the same issue).
  - Depending on the answer they should mention that a [Zendesk tag](https://about.gitlab.com/handbook/support/workflows/cmoc_workflows.html#create-zendesk-tag) is created for the purpose of this issue. **Note** that we **shouldn't** actually create a tag, this is just checking if they're following procedure.
- **IM**: Inform the practicing **CMOC** that the SRE confirmed that we are confident we've discovered the cause of the issue and that a single user is causing these issues by sending too many requests the GitLab API. Let the practicing **CMOC** know we will provide details for the practicing **CMOC** to contact them after the incident and for now that we are going to have SRE block these requests in Cloudflare. (Contacting this user will not be part of this practice event).
- **IM**: Let the practicing **CMOC** know that we would like to post a status page update but that the state should still be "Identified".
- **IM**: Inform the practicing **CMOC** that some time has passed and that we wish to move the next stage with the status page, it should be "Monitoring". Ask them to review the status page draft in the slack channel.
  - Double-check if the actual note exceeds twitter's character limit and show the practicing **CMOC** how they can check via [tweetdeck](https://tweetdeck.twitter.com/)
- **IM**: Let the practicing **CMOC** know that the status page is ready to be resolved.

Congratulations, you have successfully completed a practice scenario!

Please post any feedback on the actual scenario on the main CMOC practice event issue.
