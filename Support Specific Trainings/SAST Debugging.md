### 1. Training description
--------------------
This is an short introduction into debugging SAST issues. It includes things that you might have to look out for, files involved during a scan

### 2. Prerequisites
--------------------
Important links:

- Documentation: https://docs.gitlab.com/ee/user/application_security/sast/

- Analyzer images: https://gitlab.com/security-products

- Source Code: https://gitlab.com/gitlab-org/security-products/analyzers/

- How to use the analyzers: https://gitlab.com/gitlab-org/security-products/analyzers/common

- Include template: https://gitlab.com/gitlab-org/gitlab/-/tree/master/lib/gitlab/ci/templates/Jobs

- CI/CD Variables: https://docs.gitlab.com/ee/user/application_security/sast/#available-cicd-variables

- Sample test projects configured with scans: https://gitlab.com/gitlab-gold/secure-scanner-testbed

### 3. Video link
--------------------
SAST Debugging Tips video: https://www.youtube.com/watch?v=fs2fpNEa9Lg&list=PL05JrBw4t0Kq30AAIrAhnxip2UIPrEiLC&index=3&pp=sAQB


### 4. Troubleshooting Steps
--------------------
Add the steps required for debugging issues
